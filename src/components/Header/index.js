import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import { ButtonImage, Menu, PageLoader, MenuItem } from '../Material';
import Img from '../Img';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

import { auth } from '../../firebase';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts';

const styles = theme => {console.log(theme); return({
    flex: {
      flexGrow: 1,
      padding: "0 7%"
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    AppBar: {
        backgroundColor: 'transparent',
        boxShadow: 'none',
        color: theme.palette.primary.main
    },
    paper: {
        position: 'absolute',
        width: "90%",
        height: "80%",
        border: 'none',
        outline: 'none',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        margin: theme.spacing.unit * 6,
      },
      navList: {
        borderRight: "1px solid #e0e0e0",
        position: "absolute",
        height: "85%",
        width: "20%",
      },
      contentSettings: {
        position: "absolute",
        height: "85%",
        width: "80%",
        left: "20%"
      },
      footerSettings:{
        position: "absolute",
        borderTop: "1px solid #e0e0e0",
        width: "100%",
        top: "85%",
        height: "15%",
        textAlign: 'right',
        display: "flex",
        justifyContent: 'flex-end',
        alignItems: 'center'
      },
      button: {
          marginRight: 20
      }
  })};

class Header extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            loading: true,
            openSettings: false
        }
    }
    handleCloseSettings = ()=>{
        this.setState({openSettings: false});
    }
    render() {
        const {classes} = this.props;
        const listItems = [
            // {
            //     label: "Configuración",
            //     icon: "settings",
            //     action: ()=>{
            //         this.setState({openSettings: true});
            //     }
            // },
            {
                label: "Cerrar Sesión",
                icon: "exit_to_app",
                action: ()=>{
                    auth.doSignOut();
                }
            }
        ];
        return (
            <div>
                <AppBar position="static" className={classes.AppBar}>
                    {/* <PageLoader loading={this.state.loading} /> */}
                    <Toolbar>
                        <Grid container className={classes.flex}>
                            <Grid
                                container
                                spacing={16}
                                alignItems={"center"}
                                direction={"row"}
                                justify={"space-between"} >
                                <Grid 
                                    container xs
                                    alignItems={"center"}
                                    direction={"row"}
                                    justify={"flex-start"} >
                                        
                                </Grid>
                                <Grid 
                                    container xs md={6}
                                    alignItems={"center"}
                                    direction={"row"}
                                    justify={"center"} >
                                        <Img height="50" style={{padding: "35px 0"}} src="/assets/logo.jpeg" />
                                </Grid>
                                <Grid 
                                    container xs
                                    alignItems={"center"}
                                    direction={"row"}
                                    justify={"flex-end"} >

                                    <Menu
                                        onClickComponente={
                                            <ButtonImage 
                                                url="https://s3.us-east-2.amazonaws.com/aritex-prod/aritexplaceholder.jpg" />
                                        }>
                                        <MenuItem listElements={listItems} />
                                    </Menu>

                                </Grid>
                            </Grid>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.state.openSettings}
                    onClose={this.handleClose} >
                    <div className={classes.paper}>
                        <Grid container className={classes.demo} justify="center" spacing={8}>
                            <Grid item xs={12}>
                                <div className={classes.navList}>
                                    <List component="nav">
                                        <ListItem button>
                                        <ListItemIcon>
                                            <InboxIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Inbox" />
                                        </ListItem>
                                        <ListItem button>
                                        <ListItemIcon>
                                            <DraftsIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Drafts" />
                                        </ListItem>
                                    </List>
                                    {/* <Divider />
                                    <List component="nav">
                                        <ListItem button>
                                        <ListItemText primary="Trash" />
                                        </ListItem>
                                        <ListItem button component="a" href="#simple-list">
                                        <ListItemText primary="Spam" />
                                        </ListItem>
                                    </List> */}
                                </div>
                                <div className={classes.contentSettings}>
                                    
                                </div>
                                <div className={classes.footerSettings}>
                                    <Button variant="contained" className={classes.button} onClick={this.handleCloseSettings}>
                                        Cerrar
                                    </Button>
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </Modal>
            </div>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};  

export default withStyles(styles)(Header);


// handleMenu = event => {
//     this.setState({ anchorEl: event.currentTarget });
//   };

//   handleClose = () => {
//     this.setState({ anchorEl: null });
//   };  
        
// const { classes } = this.props;
// const { anchorEl } = this.state;
// const open = Boolean(anchorEl); 

        //   <Toolbar>
        //     <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
        //       <MenuIcon />
        //     </IconButton>
        //     <Typography variant="title" color="inherit" className={classes.flex}>
        //       Photos
        //     </Typography>
        //      <div>
        //         <IconButton
        //           aria-owns={open ? 'menu-appbar' : null}
        //           aria-haspopup="true"
        //           onClick={this.handleMenu}
        //           color="inherit"
        //         >
        //           <AccountCircle />
        //         </IconButton>
        //         <Menu
        //           id="menu-appbar"
        //           anchorEl={anchorEl}
        //           anchorOrigin={{
        //             vertical: 'top',
        //             horizontal: 'right',
        //           }}
        //           transformOrigin={{
        //             vertical: 'top',
        //             horizontal: 'right',
        //           }}
        //           open={open}
        //           onClose={this.handleClose}
        //         >
        //           <MenuItem onClick={this.handleClose}>Profile</MenuItem>
        //           <MenuItem onClick={this.handleClose}>My account</MenuItem>
        //         </Menu>
        //       </div>
        //   </Toolbar>