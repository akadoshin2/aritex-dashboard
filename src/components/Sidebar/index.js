import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Route, Link } from "react-router-dom";
import Routes from '../../routes';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Icon, Divider } from '../../../node_modules/@material-ui/core';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    linkPage:{
        textDecoration: 'none',
        color: theme.typography.display1.color
    },
    navList: {
        padding: 0
    },
    labelListItemText: {
        paddingRight: 15
    },
    listItem: {
        '&.active': {
          backgroundColor: theme.palette.primary.main,
          '& $primary, & $icon': {
            color: theme.palette.common.white,
          },
        },
      },
      primary: {},
      icon: {},
});

const LinkListItem = ({ to, classes, children, index }) => (
    <Route
      exact path={to}
      children={({ match }) => (
            <Link to={to} className={classes.linkPage}>
                {(index!=1)&&(<Divider />)}
                <ListItem button className={`${classes.listItem} ${match ? "active " : ""}`}>
                    {children} 
                </ListItem>
            </Link>
      )}
    />);

class Sidebar extends Component {
    render() {
        const { classes, StyleContainer } = this.props;
        return (
            <div className={classes.root} style={StyleContainer}>
                <List component="nav" className={classes.navList}>
                    {Routes.map((page, key)=>{
                        if(page.enabled&&page.isSidebar){
                            return(
                                <LinkListItem index={key} key={key} to={page.path} classes={classes}>
                                    <Icon className={classes.icon}>{page.icon}</Icon>
                                    <ListItemText className={classes.labelListItemText} classes={{ primary: classes.primary }} primary={page.label} />
                                </LinkListItem>
                            )
                        }
                    })}
                </List>
            </div>
        );
    }
}

Sidebar.propTypes = {
    
}

export default withStyles(styles)(Sidebar);