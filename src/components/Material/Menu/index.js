import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import Fade from '@material-ui/core/Fade';

const styles = theme => ({

});

class MenuMaterial extends Component{
    constructor(props){
        super(props);
        this.state = {
            anchorEl: null,
        };
        this.renderOnClickComponente = this.renderOnClickComponente.bind(this);
        this.renderChildren = this.renderChildren.bind(this);
    }
    handleClick = event => {
        this.setState({ anchorEl: event.currentTarget });
    };
    
    handleClose = () => {
        this.setState({ anchorEl: null });
    };
    renderOnClickComponente(){
        return React.Children.map(this.props.onClickComponente, child => {
            return React.cloneElement(child, {
                onClick: this.handleClick
            });
        }); 
    }
    renderChildren(){
        return React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                onClose: this.handleClose
            });
        }); 
    }
    render(){
        const { anchorEl } = this.state;
        return(
            <div>
                <div>
                    {this.renderOnClickComponente()}
                </div>
                <Menu
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                    disableAutoFocusItem={true}
                    TransitionComponent={Fade} >
                    {this.renderChildren()}
                </Menu>
            </div>
        );
    }
}

MenuMaterial.propTypes = {
    children: PropTypes.element
}

export default withStyles(styles)(MenuMaterial);