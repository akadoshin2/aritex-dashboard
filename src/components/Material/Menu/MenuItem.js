import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import Icon from '@material-ui/core/Icon';
import Avatar from '@material-ui/core/Avatar';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

const styles = theme => ({
    menuItem: {
        '&:focus': {
          backgroundColor: theme.palette.primary.main,
          '& $primary, & $icon': {
            color: theme.palette.common.white,
          },
        },
      },
      primary: {},
      icon: {},
});

class MenuItemMaterial extends Component{
    constructor(props){
        super(props);
        this.state = {
            open: false
        }
    }
    handleClick = () => {
        this.setState(state => ({ open: !state.open }));
      };
    render(){
        const { classes, listElements, onClose } = this.props;
        return(
            <div>
                {listElements.map((item, key) => (
                    <MenuItem onClick={()=>{ item.action(); onClose(); }} key={key}>
                        <ListItemIcon>
                            <Icon>{item.icon}</Icon>
                        </ListItemIcon>
                        <ListItemText primary={item.label} />
                    </MenuItem>
                ))}
            </div>
            // <MenuItem button onClick={this.handleClick}>
            //     <ListItemIcon>
            //         <Icon>{icon}</Icon>
            //     </ListItemIcon>
            //     <ListItemText inset primary="Inbox" />
            //     {this.state.open ? 
            //         <Icon>expand_less</Icon>: 
            //         <Icon>expand_more</Icon>}
            // </MenuItem>
            // <Collapse in={this.state.open} timeout="auto" unmountOnExit>
            //         <List component="div" disablePadding>
            //         <ListItem  button className={classes.nested}>
            //             <ListItemIcon>
            //                 <Icon>{icon}</Icon>
            //             </ListItemIcon>
            //             <ListItemText inset primary="Starred" />
            //         </ListItem >
            //         </List>
            //     </Collapse>
            // </div>
        );
    }
}

MenuItemMaterial.propTypes = {

}

export default withStyles(styles)(MenuItemMaterial);