import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
    loadingLinearProgress: {
        position: 'fixed',
        width: '100%',
        height: 2.3,
        top: 0, left: 0
    }
});

class PageLoaderMaterial extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const { classes, loading } = this.props;
        return(
            loading && (<LinearProgress variant="query" className={classes.loadingLinearProgress} />)
        );
    }
}

PageLoaderMaterial.propTypes = {

}

export default withStyles(styles)(PageLoaderMaterial);