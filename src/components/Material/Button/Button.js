import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { PositionRelative } from '../../../styles';

const styles = theme => ({
    button: {
        borderRadius: 2,
        boxShadow: "0 6px 11px 0 rgba(0,0,0,0.24)",
        padding: '10px 22px'
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
      }
});

class ButtonMaterial extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const { classes, loading, disabled, ...restProps } = this.props;
        return(
            <PositionRelative>
                <Button disabled={loading} className={classes.button} {...restProps}>
                    {this.props.children}
                </Button>
                {(!disabled) && (loading && <CircularProgress size={24} className={classes.buttonProgress} />)}
            </PositionRelative>
        );
    }
}

export default withStyles(styles)(ButtonMaterial);