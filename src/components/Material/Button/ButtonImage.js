import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Icon from '@material-ui/core/Icon';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { BackgroundImagen } from '../../../styles';

const styles = theme => ({
    ButtonImage: {
        flex: "0 0 auto",
        width: 68,
        height: 68,
        padding: 0,
        fontSize: "1.5rem",
        textAlign: "center",
        transition: "background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
        borderRadius: "50%",
        '&:hover':{
            backgroundColor: theme.palette.action.hover
        }
    },
    menuItem: {
        '&:focus': {
          backgroundColor: theme.palette.primary.main,
          '& $primary, & $icon': {
            color: theme.palette.common.white,
          },
        },
      },
      primary: {},
      icon: {},
});

class ButtonImage extends Component{
    constructor(props){
        super(props);
        this.state = {anchorEl: null}
    }
    render(){
        const { classes, url, ...restProps } = this.props;
        return(
            <ButtonBase
                centerRipple={true} 
                color="primary"
                className={classes.ButtonImage}
                {...restProps} >
                    <BackgroundImagen radius="50%" height="40px" width="40px" url={url} />
            </ButtonBase>
        );
    }
}

export default withStyles(styles)(ButtonImage);