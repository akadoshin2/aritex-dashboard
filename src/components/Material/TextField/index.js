import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { MDCTextField } from "@material/textfield";
import {MaterialIcons} from '../../../styles';

class TextField extends Component {
    constructor(props){
        super(props);
        this.id = `tf-${Math.random().toString(36).substr(2, 9)}`;
        this.switchType = this.switchType.bind(this);
        
    }
    static defaultProps = {}
    componentDidMount() {
        const {value, onChange} = this.props;
        this.MDCComponent = new MDCTextField(this.refs.fieldRef);
        this.MDCComponent.input_.onchange = onChange; 
        for(const key in this.props) 
            if(this.MDCComponent[key]!==undefined) this.MDCComponent[key] = this.props[key];
    }
    componentWillReceiveProps(nextProps) {
        this.MDCComponent.value = nextProps.value;
    }
    showIcon(icon){
        if(icon) return(<MaterialIcons className="mdc-text-field__icon" style={{userSelect: "none", pointerEvents: "none"}} tabIndex="-1" role="button">{icon}</MaterialIcons>);
    }
    switchType(){
        const {
            multiline,
            typeStyle,
            label,
            inputStyle,
            containerStyles,
            classNameInput,
            className,
            type,
            icon,
            autoFocus,
            onChange,
        } = this.props;
        const _RootClassName = `mdc-text-field ${icon&&('mdc-text-field--with-leading-icon')} ${typeStyle&&(`mdc-text-field--${typeStyle}`)} ${className}`;
        switch (typeStyle) {
            case "outlined":
                return(
                    <div ref="fieldRef" className={_RootClassName} style={containerStyles}>
                        {this.showIcon(icon)}
                        <input multiline={multiline} autoFocus={autoFocus} type={type} id={this.id} className={`mdc-text-field__input ${classNameInput}`} style={inputStyle}/>
                        <label htmlFor={this.id} className="mdc-floating-label">{label}</label>
                        <div className="mdc-notched-outline">
                            <svg style={{left: 0}}>
                                <path className="mdc-notched-outline__path"/>
                            </svg>
                        </div>
                        <div className="mdc-notched-outline__idle"></div>
                    </div>
                );
    
            default:
                return(
                    <div ref="fieldRef" className={_RootClassName} style={containerStyles}>
                        {this.showIcon(icon)}
                        <input multiline={multiline} type="text" id={this.id} className="mdc-text-field__input" />
                        <label className="mdc-floating-label" htmlFor={this.id}>{label}</label>
                        <div className="mdc-line-ripple"></div>
                    </div>
                );
        }
    }
    render(){
        return(this.switchType());
    }
}

TextField.propTypes = {
    //It is the label of the Input
    label: PropTypes.string.isRequired,
    //The class name that is added to the parent of the elements
    className: PropTypes.string,
    //The classes that are added to the input
    classNameInput: PropTypes.string
};

export default TextField;