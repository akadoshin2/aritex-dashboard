//import "material-components-web/dist/material-components-web.css";
//import "material-design-icons-iconfont/dist/material-design-icons.css";
import TextField from './TextField';
import Button from './Button/Button';
import ButtonImage from './Button/ButtonImage';
import Menu from './Menu';
import MenuItem from './Menu/MenuItem';
import List from './Lists/list';
import PageLoader from './PageLoader';

export {
    TextField,
    Button,
    ButtonImage,
    Menu,
    MenuItem,
    List,
    PageLoader
};

// import React, { Component } from 'react';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';

// const styles = theme => ({

// });

// class MenuMaterial extends Component{
//     constructor(props){
//         super(props);
//     }
//     render(){
//         return(null);
//     }
// }

// MenuMaterial.propTypes = {

// }

// export default withStyles(styles)(MenuMaterial);