import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {Menu, MainButton, ChildButton} from 'react-mfb';
import './buttonstyles.min.css';
import FullScreenDialog from '../FullScreenDialog';
import NewNotification from '../NewNotification';
import NewCarousel from '../NewCarousel';
import NewUser from '../NewUser';

import { sendNotifications } from '../../firebase/Messaging';

const styles = theme => ({
  root: {
  },
  buttonFab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 5,
    right: theme.spacing.unit * 14,
  }
});

class FloatingActionButton extends React.Component {
  state = {
    openNewUser: false,
    openNewNotification: false,
    openNewCarousel: false,
  }
  openUser = ()=>{
    this.setState({openNewUser: !this.state.openNewUser}); 
  }
  openNotification = ()=>{
    this.setState({openNewNotification: !this.state.openNewNotification}); 
  }
  openCarousel = ()=>{
    this.setState({openNewCarousel: !this.state.openNewCarousel}); 
  }
  sendNotification = ({msg, audience, title, disabled, draft}, restart) => {
    sendNotifications({draft, body: msg, to: audience, title});
    this.setState({openNewNotification: false}, restart); 
  }
  render() {
    const { openNewUser, openNewNotification, openNewCarousel } = this.state;
    const { classes } = this.props;
    return (
        <div>
            <Menu effect={'slidein'} method={'hover'} position={'br'}>
                <MainButton iconResting="ion-md-add" iconActive="ion-md-close" />
                <ChildButton
                    icon="ion-md-person-add"
                    label="Añadir Usuario"
                    onClick={this.openUser} />
                <ChildButton
                    icon="ion-md-notifications-outline"
                    label="Añadir Notificación"
                    onClick={this.openNotification}  />
                <ChildButton
                    icon="ion-md-albums"
                    label="Añadir Carousel"
                    onClick={this.openCarousel}  />
            </Menu>
            <NewNotification sendNotification={this.sendNotification} open={openNewNotification} handleClose={this.openNotification} />
            <NewUser open={openNewUser} handleClose={this.openUser} />
            <NewCarousel open={openNewCarousel} handleClose={this.openCarousel} />
        </div>
    );
  }
}

FloatingActionButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FloatingActionButton);