import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {Line} from 'react-chartjs-2';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June'],
    datasets: [
      {
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [65, 59, 80, 81, 56, 55]
      }
    ]
  };

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    chartContainer: {
        height: 150,
        position: 'relative',
        padding: "15px 30px",
        //backgroundColor: theme.palette.divider,
        //borderBottom: `1px solid ${theme.palette.divider}`
    },
    Paper: {
        margin: 30,
        padding: 12,
        boxShadow: "1px 2px 7px 0 rgba(0,0,0,0.1)"
    }
});

class Statistics extends Component {
    constructor(props){
        super(props);
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.Paper}>
                            <Line data={data} options={{
                                maintainAspectRatio: false,
                                legend:{
                                    display:false
                                }
                            }}/>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

Statistics.propTypes = {
    
}

export default withStyles(styles)(Statistics);