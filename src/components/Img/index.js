import React, {Component} from "react";
import {ContainerClear} from '../../styles';
import defaultImg from './defaultImagen.svg';

class Img extends Component {
  constructor(props) {
    super(props);
    this.defaultImgRef = React.createRef();
    this.state = { status: false };
  }

  handleImageLoaded() {
    this.setState({ status: true }, ()=>{
        this.defaultImgRef.current.remove();
    });
  }

  handleImageErrored() {
    this.setState({ status: "undefined" });
  }

  render() {
      console.log(this.state.status)
    const { ...other } = this.props;
    return(
        <ContainerClear>
            <span style={{display: this.state.status?'contents':'none'}}>
                <img
                    {...other}
                    onLoad={this.handleImageLoaded.bind(this)}
                    onError={this.handleImageErrored.bind(this)}
                />
            </span>
            <span style={{display: !this.state.status?'contents':'none'}} ref={this.defaultImgRef}>
                <img
                    style={{
                        width: '15%',
                        background: '#ffffffdb',
                        padding: '5%',
                        borderRadius: '17px',
                    }}
                    src={defaultImg}
                />
            </span>
        </ContainerClear>
    );
  }
}
export default Img;