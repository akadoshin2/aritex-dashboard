import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FullScreenDialog from '../FullScreenDialog';
import Typography from '@material-ui/core/Typography';

import {TextField} from '../Material';
import TextFieldMaterial from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import ArrowDropDown from '@material-ui/icons/ExpandMore';
import InputAdornment from '@material-ui/core/InputAdornment';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import Files from 'react-files';
import Paper from '@material-ui/core/Paper';
import { doCreateUserWithEmail } from '../../firebase/Auth';
import { setUser } from '../../firebase/Database';

const styles = theme => ({
    container: {
        padding: "40px 13%",
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    inputStyle: {
       width: "100%"
    },
    menu: {
        width: 200,
    },
    button: {
        backgroundColor: "#FFC107",
        color: "#FFF",
        margin: "20px 10px",
    },
    inputCreateService:{
        outline:'none', 
        border:'1px solid #E0E0E0', 
        height:'52px', 
        paddingLeft:'24px', 
        paddingRight: '8px',
        paddingTop:'2px', 
        borderRadius:'3px',
        display: "flex",
        alignItems: "center",
        marginTop: 15
    },
});

class NewNotification extends React.Component {
    state = {
        disabled: true,
        newUserType: "Admin",
        fullName: "",
        cc: "",
        codigoAsesor: "",
        email: "",
        password: "",
        country: "",
        city: "",
        Czone: "",
        tel2: "",
        tel1: "",
        picture: [],
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        }, () => {
            const {fullName, cc, codigoAsesor, email, password, country, city, Czone, tel1, tel2} = this.state;
            if(fullName!=""&&cc!=""&&codigoAsesor!=""&&email!=""&&password!=""&&country!=""&&city!=""&&Czone!="") this.setState({disabled: false});
            else this.setState({disabled: true});
        });
    };  
    handleSend = ()=>{
        const {newUserType, fullName, cc, codigoAsesor, email, password, country, city, Czone, tel1, tel2} = this.state;
        doCreateUserWithEmail(email, password).then(res=>{
            let uid = res.user.uid;
            console.log(res, newUserType);
            setUser({
                uid,
                message: `Bienvenido ${fullName}`,
                is_client: (newUserType==="Cliente"),
                clients: [],
                orders: [],
                seller: {
                    _id: uid,
                    is_client: (newUserType==="Cliente"),
                    seller_code: codigoAsesor,
                    phone1: tel1,
                    phone2: tel2,
                    zone: Czone,
                    city,
                    country,
                    cc,
                    name: fullName,
                    user_id: uid,
                    total_sells: 0,
                    status: 1
                },
                user: {
                    _id: uid,
                    password,
                    email,
                    screenB: [],
                    screenA: [],
                    profile: "https://s3.us-east-2.amazonaws.com/aritex-prod/aritexplaceholder.jpg",
                    status: 1,
                    admin:  (newUserType==="Admin"),
                    __v: 0,
                    seller: {
                        _id: uid,
                        is_client: (newUserType==="Asesor"),
                        seller_code: codigoAsesor,
                        phone1: tel1,
                        phone2: tel2,
                        zone: Czone,
                        city,
                        country,
                        cc,
                        name: fullName,
                        user_id: uid,
                        total_sells: 0,
                        status: 1
                    }
                }
            }, (res=>{
                console.log(res);
                this.setState({
                    disabled: true,
                    newUserType: "Admin",
                    fullName: "",
                    cc: "",
                    codigoAsesor: "",
                    email: "",
                    password: "",
                    country: "",
                    city: "",
                    Czone: "",
                    tel2: "",
                    tel1: "",
                    picture: [],
                });
            }))
        }).catch(err => {
            alert(err.message);
        });
    }
    render(){
        const serviceType = ['Admin', 'Asesor'];
        // const serviceType = ['Admin', 'Cliente', 'Asesor'];
        const {classes, open, handleClose} = this.props;
        const {disabled, fullName, cc, codigoAsesor, email, password, country, city, Czone, tel1, tel2} = this.state;
        return(
            <div>
                <FullScreenDialog toolBarColor={"#FFC107"} title={"Nuevo Usuario"} open={open} handleClose={handleClose} content={
                    <div  className={classes.container}>
                        <Grid container spacing={24}>
                            <Grid item xs={4}>
                                <Select
                                    value={this.state.newUserType}
                                    onChange={this.handleChange('newUserType')}
                                    className={classes.inputCreateService}
                                    name="newUserType"
                                >
                                    {serviceType.map((prop, key) => (
                                        <MenuItem key={key} value={prop}>
                                        {prop}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="full-name"
                                    label="Nombre Completo"
                                    value={fullName}
                                    onChange={this.handleChange('fullName')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="cc"
                                    label="Cédula"
                                    value={cc}
                                    onChange={this.handleChange('cc')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="codigoAsesor"
                                    label="Código de asesor"
                                    value={codigoAsesor}
                                    onChange={this.handleChange('codigoAsesor')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="email"
                                    typeStyle="outlined"
                                    id="email"
                                    label="Correo Electronico"
                                    value={email}
                                    onChange={this.handleChange('email')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="password"
                                    typeStyle="outlined"
                                    id="password"
                                    label="Contraseña"
                                    value={password}
                                    onChange={this.handleChange('password')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="country"
                                    label="País"
                                    value={country}
                                    onChange={this.handleChange('country')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="city"
                                    label="Ciudad"
                                    value={city}
                                    onChange={this.handleChange('city')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="Czone"
                                    label="Zona"
                                    value={Czone}
                                    onChange={this.handleChange('Czone')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="number"
                                    typeStyle="outlined"
                                    id="tel1"
                                    label="Teléfono 1"
                                    value={tel1}
                                    onChange={this.handleChange('tel1')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="number"
                                    typeStyle="outlined"
                                    id="tel2"
                                    label="Teléfono 2"
                                    value={tel2}
                                    onChange={this.handleChange('tel2')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            {/* <Grid>
                                <div style={{ width:'212px', marginRight:'24px', display:'inline-block' }}>
                                    <p style={{ marginBottom:'26px' }}>Imagen de perfil</p>
                                    <Files
                                        ref='pictures'
                                        className='files-dropzone-list'
                                        style={(!(this.state.picture.length>0))? {height: '74px', width:'74px', display: 'inline-block',}:{height:'0', width:'0'} }
                                        onChange={this.onFilesChangePicture}
                                        onError={this.onFilesErrorPicture}
                                        accepts={['image/*']}
                                        maxFileSize={10000000}
                                        minFileSize={0}
                                        clickable
                                    >
                                        {(!(this.state.picture.length>0))?
                                            <Paper style={{ height: 74,
                                                    width: 74,
                                                    paddingTop:'23px',
                                                    textAlign: 'center',
                                                    display: 'inline-block',
                                                    backgroundColor:'#e6e6e6',
                                                    cursor:'pointer'
                                                }}
                                                zDepth={0}
                                            >
                                                <MenuItem style={{ width:'23px', height:'21px' }} color={'#4a4a4a'}></MenuItem>
                                                <p style={{ fontSize:'14px', color:'rgba(0,0,0,0.84)', marginTop:'-5px'}}>Agregar</p>
                                            </Paper>:''}
                                    </Files>
                                        {/* <button onClick={this.filesRemoveAll}>Borrar todo</button> */}
                                        {/* <button onClick={this.filesUpload}>Upload</button> */}
                                    {/* {this.state.picture.length > 0 ? 
                                        <div className='files-list'>
                                            <ul style={{ paddingLeft:'0', display:'inline-block', marginBottom:'-15px' }}>{this.state.picture.map((file) =>
                                            <li className='files-list-item' key={file.id} style={{ 
                                                    listStyleType:'none', 
                                                    display:'inline-block', 
                                                    float:'left', 
                                                    marginRight:'12px',
                                                    position:'relative'
                                                }}>
                                                <div className='files-list-item-preview'>
                                                {file.preview.type === 'image'
                                                ? <img className='files-list-item-preview-image' src={file.preview.url} style={{ width:'74px', height:'74px' }}/>
                                                : <div className='files-list-item-preview-extension'>{file.extension}</div>}
                                                </div>
                                                <div
                                                id={file.id}
                                                className='files-list-item-remove'
                                                onClick={this.filesRemoveOnePicture.bind(this, file)} // eslint-disable-line
                                                ><p style={{ cursor:'pointer', color:'red', fontSize:'12px', bottom:'72%', position:'absolute', left:'86%', backgroundColor:'#ff5722', borderRadius:'50%', padding:'1px 6px', paddingTop:'3px', color:'white' }}>X</p></div>
                                            </li>
                                            )}</ul>
                                        </div>
                                        : null
                                    }
                                </div>
                            </Grid> */}
                            <Grid item xs={12}>
                                <Button disabled={disabled} variant="contained" onClick={this.handleSend} className={classes.button}>
                                    CREAR
                                </Button>
                                <Button disabled={disabled} variant="contained"  className={classes.button}>
                                    CANCELAR
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                } />
            </div>
        );
    }
}

NewNotification.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewNotification);