import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Scrollbars } from 'react-custom-scrollbars';

import Sidebar from '../Sidebar';
import FloatActionButton from '../FloatActionButton';

const styles = theme => ({
    root: {
        flexGrow: 1,
        margin: "0 7%"
      },
      paper: {
        borderRadius: 10,
        overflow: 'hidden',
        boxShadow: "0 5px 5px 0 rgba(0,0,0,0.1)",
        color: theme.palette.text.secondary,
      },
      mainComponent: {
        minHeight: '87vh',
        maxHeight: '87vh',
        overflow: "auto"
      }
});

class Main extends Component {
    render() {
        const sideBarActive = true;
        const { classes, children } = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={12} sm={4} md={3}>
                        <Paper className={classes.paper}>
                            <Sidebar StyleContainer={{borderRadius: 10}} active={sideBarActive} />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} sm={8} md={9}>
                        <Paper className={`${classes.paper} ${classes.mainComponent}`}>
                            <Scrollbars autoHide autoHideTimeout={1500} thumbMinSize={40} autoHideDuration={200} style={ {width: '100%', minHeight: '87vh'} } >
                                {children}
                            </Scrollbars>
                        </Paper>
                    </Grid>
                </Grid>
                <FloatActionButton />
            </div>
        );
    }
}

Main.propTypes = {
    children: PropTypes.element.isRequired
}

export default withStyles(styles)(Main);