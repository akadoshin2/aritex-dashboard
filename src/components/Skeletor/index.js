import React, { Component } from 'react';
import styled, {keyframes} from 'styled-components';

const ReactSkeletonProgress = keyframes`
    from {background-position: -250% 0;}
    to {background-position: calc(250%) 0;}
`;

const ReactSkeleton = styled.div`
    display: grid;
    animation: ${ReactSkeletonProgress} 1.2s ease-in-out infinite;
    background-color: #eee;
    background-image: linear-gradient(90deg, #eee, #f7f7f7, #eee);
    background-size: 60% 100%;
    background-repeat: no-repeat;
    line-height: 1;
    width: 100%;
    height: 100%;
    grid-template-columns: repeat(${props=>props.column||'10'}, 1fr);
    grid-template-rows: 1fr; 
    grid-row-gap: 15px;
`;

const ReactSkeletonContainer = styled.div`
    overflow: hidden;
    display: block;
    position: relative;
`;

const ReactSkeletonLineTypeStyle = styled.div`
    overflow: hidden;
    display: block;
    position: relative;
    border: ${props=>props.border/2}px solid ${props=>props.styleColor||'#FFF'};
    border-left-width: ${props=>props.border/2}px;
    border-right-width: ${props=>props.border/2}px;
    &:before{
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        border-radius: 17px;
        border: ${props=>props.border}px solid ${props=>props.styleColor||'#FFF'};
        left: -${props=>props.border}px;
        top: -${props=>props.border}px;
    }
`;

const SkeletorBoxStyled = styled.span`
    background-color: ${props=>props.styleColor||'#FFF'};
    min-height: ${props=>props.styleHeight||'12px'};
    grid-column-start: ${props=>props.columnStart||'1'};
    grid-column-end: ${props=>props.columnEnd||'1'};
`;

class Skeletor extends Component {
    constructor(props){
        super(props);
    }
    renderChildrens = () => {
        const { colums, color, children, height, showSkeleton, ...restProps } = this.props;
        if(!showSkeleton) return <span></span>;
        if(React.Children.count(children)>1){
            return React.Children.map(children, (child, index) => {
                if(child.type === SkeletorBox)
                    return <ReactSkeleton column={colums[index]} key={index}>
                                {React.cloneElement(child, {
                                    colums: colums[index],
                                    color,
                                    height
                                })}
                            </ReactSkeleton>
                    return child;
                }
            );
        }
        return (
            <ReactSkeleton column={colums} {...restProps}>
                {React.cloneElement(this.props.children, {
                    colums,
                    color,
                    height
                })}
            </ReactSkeleton>
        );
    }
    render(){
        return this.renderChildrens();
    }
}

export class SkeletorBox extends Component {
    constructor(props){
        super(props);
    }
    render(){
        const { color, height, children, colums } = this.props;
        let childsCount = React.Children.count(children);
        let childrsValid = React.Children.map(children, (child, key) => {
            if(child.props.empty){
                return <SkeletorBoxStyled styleHeight={height} columnStart={(key+1)} columnEnd={key+2} styleColor={color} key={key} />
            }
            return React.cloneElement(child, {
                key, index: key,
                color,
                height
            });
        })
        childrsValid.push(<SkeletorBoxStyled styleHeight={height} columnStart={(childsCount+1)} columnEnd={colums+1} styleColor={color} key={colums} />);
        return childrsValid;
    }
}

export class SkeletorLineType extends Component {
    constructor(props){
        super(props);
        console.log(props);
    }
    render(){
        const { border = 8, color } = this.props;
        return(
            <ReactSkeletonLineTypeStyle border={border} styleColor={color} />
        );
    }
}

export class SkeletonContainer extends Component {
    constructor(props){
        super(props);
    }
    render(){
        const { children, colums } = this.props;
        return(<div></div>);
    }
}

export class SkeletonShowContent extends Component {
    render(){
        const { children, hidden, ...restProps } = this.props;
        return <div {...restProps}>{hidden||children}</div>;
    }
}

export default Skeletor;
