import React, { Component } from 'react';
import './index.css';

/**
 * ignore: true|false
 * 
 */

let counter = 0;

const css = (id, color = '#eee', highlight = '#f5f5f5') =>
`.react-loading-theme-${id} .react-loading-skeleton {
    background-color: ${color};
    background-image: linear-gradient(90deg, ${color}, ${highlight}, ${color});
}`;

const createStyleElement = (css) => {
    const element = document.createElement('style');
    element.textContent = css;
    document.head.appendChild(element);
    return element;
};

class Skeletor extends Component {
    static defaultProps = {
        count: 1,
        duration: 1.2,
        width: null,
        wrapper: null
    };
    constructor(props){
        super(props);
        this.id = counter++;
    }
    componentWillMount() {
        this.style = createStyleElement(css(
            this.id, this.props.color, this.props.highlightColor
        ));
    }
    componentWillUnmount() {
        this.style.remove();
    }
    renderChildrens = ({childs = []} = {}, {children, dataEach} = this.props) => {
        dataEach.map((data, key) => childs.push(children({data, key})));
        return childs;
    }
    renderDummynChildrens = ({childs = []} = {}, {children, dummyResults, dummyData} = this.props) => {
        for(let i=0;i<dummyResults;i++) childs.push(children({data: dummyData, key: i}));
        return childs;
    }
    isChildComponent = (component) => {
        return Boolean(component.props.SkeletonIsChild);
    }
    isParentComponent = (component) => {
        return Boolean(component.props.children.length);
    }
    getContainerParent = (component) => {
        let containerParent = React.cloneElement(component, {children: null});
        let componentParent = component;
        do {
            React.Children.map(componentParent.props.children, actual => {
                componentParent = React.cloneElement(actual);
                containerParent = React.cloneElement(containerParent, {
                    children: React.cloneElement(componentParent, {children: null})
                });
            });
        } while(componentParent.props.children&&!this.isParentComponent(componentParent));
        return React.cloneElement(containerParent);
    }
    getParent = (component) => {
        let componentParent = component;
        do {
            React.Children.map(componentParent.props.children, actual => {
                componentParent = React.cloneElement(actual);
            });
        } while(componentParent.props.children&&!this.isParentComponent(componentParent));
        return React.cloneElement(componentParent);
    }
    renderChildrenComponent = ({ dataEach } = this.props) => {
        console.clear();
        if(Boolean(dataEach.length)) return this.renderChildrens();
        return React.Children.map(this.renderDummynChildrens(), component => {
            return React.Children.map(this.getParent(component), parent => {
                let newParent = parent;
                newParent = React.cloneElement(parent, {
                            children: React.Children.map(parent.props.children, children => {
                                console.log(children);
                                return React.cloneElement(children, {
                                    className: `react-loading-theme-${this.id}`,
                                    style: {
                                        ...children.props.style,
                                        ...children.props.SkeletonStyle,
                                    },
                                    children: React.cloneElement(<span className="react-loading-skeleton" style={{
                                                animation: "progress " + String(this.props.duration) + "s ease-in-out infinite"
                                            }} />, {
                                        children: children.props.children
                                    })
                                });
                            })
                        })
                return React.cloneElement(this.getContainerParent(component), {
                    children: newParent
                });
            });
        });
    }
    render(){
        return this.renderChildrenComponent();
    }
}

export default Skeletor;