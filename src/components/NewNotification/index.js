import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FullScreenDialog from '../FullScreenDialog';
import Typography from '@material-ui/core/Typography';

import {TextField} from '../Material';
import TextFieldMaterial from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: "center",
        flexDirection: 'column',
        margin: "40px 10%"
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    inputStyle: {
        margin: "10px 15px",
        width: 200,
    },
    menu: {
        width: 200,
    },
    button: {
        backgroundColor: "#FFC107",
        color: "#FFF",
        margin: "20px 10px",
    },
    inputCreateService:{
        width: 200,
        marginLeft: 13,
        outline:'none', 
        border:'1px solid #E0E0E0', 
        height:'52px', 
        paddingLeft:'24px', 
        paddingRight: '8px',
        paddingTop:'2px', 
        borderRadius:'3px',
        display: "flex",
        alignItems: "center",
        marginTop: 15
    },
});

const currencies = [
    {
      value: 'notificaciones',
      label: 'TODOS',
    },
    {
      value: 'asesores',
      label: 'ASESORES',
    },
    {
      value: 'clientes',
      label: 'CLIENTES',
    },
  ];

class NewNotification extends React.Component {
    state = {
        msg: '',
        audience: 'notificaciones',
        title: '',
        disabled: true
      };    
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        }, () => {
            const {msg, audience, title} = this.state;
            if(msg!=""&&audience!=""&&title!="") this.setState({disabled: false});
            else this.setState({disabled: true});
        });
    };  
    restart = () => {
        this.setState({msg: "", audience: "", title: "", disabled: true});
    }
    render(){
        const {classes, open, handleClose, sendNotification} = this.props;
        const {msg, audience, title, disabled} = this.state;
        return(
            <div>
                <FullScreenDialog toolBarColor={"#FFC107"} title={"Nueva Notificación"} open={open} handleClose={handleClose} content={
                    <div className={classes.container}>
                    <Typography variant="subheading" gutterBottom>
                        Información de mensaje
                    </Typography>
                    <TextField
                        type="text"
                        typeStyle="outlined"
                        id="title-flexible"
                        label="Titulo"
                        value={title}
                        onChange={this.handleChange('title')}
                        className={classes.inputStyle}
                        containerStyles={{width: "45%"}}
                        margin="normal"
                    />
                    <TextField
                        id="msg-multiline"
                        label="Mensaje"
                        typeStyle="outlined"
                        multiline
                        fullWidth
                        rows="3"
                        value={msg}
                        onChange={this.handleChange('msg')}
                        className={classes.inputStyle}
                        containerStyles={{width: "60%"}}
                        margin="normal"
                    />
                    <Select
                        id="select-audience"
                        className={classes.inputCreateService}
                        value={audience}
                        onChange={this.handleChange('audience')}
                        SelectProps={{
                            MenuProps: {
                            className: classes.menu,
                            },
                        }}
                    >
                        {currencies.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </Select>
                    {/* <TextFieldMaterial
                        id="select-audience"
                        select
                        label="Audiencia"
                        className={classes.inputStyle}
                        value={audience}
                        onChange={this.handleChange('audience')}
                        SelectProps={{
                            MenuProps: {
                            className: classes.menu,
                            },
                        }}
                        margin="normal"
                        >
                        {currencies.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextFieldMaterial> */}
                    <div>
                        <Button onClick={()=>sendNotification({msg, audience, title, disabled, draft: false}, this.restart)} disabled={disabled} variant="contained"  className={classes.button}>
                            ENVIAR
                        </Button>
                        <Button onClick={()=>sendNotification({msg, audience, title, disabled, draft: true}, this.restart)} disabled={disabled} variant="contained"  className={classes.button}>
                            BORRADOR
                        </Button>
                    </div>
                  </div>
                } />
            </div>
        );
    }
}

NewNotification.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewNotification);