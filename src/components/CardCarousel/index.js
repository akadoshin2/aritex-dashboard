import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
import Icon from '@material-ui/core/Icon';
import Grow from '@material-ui/core/Grow';
import Collapse from '@material-ui/core/Collapse';
import TextFieldMaterial from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import blue from '@material-ui/core/colors/blue';
import moment from 'moment';
import { Button, MenuItem, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, FormGroup, FormControlLabel, Switch, Select } from '../../../node_modules/@material-ui/core';
import { TextField } from '../Material';
import { deleteCategoriesImg, getCategories } from '../../firebase/Database';

const styles = theme => ({
  card: {
    display: 'flex',
    flexDirection: 'column',
    margin: 12,
  },
  header: {
    flex: 1,
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 200,
    height: 156,
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  collapse: {
    //borderTop: `solid 1px ${theme.palette.divider}`,
  },
  button: {
    marginLeft: theme.spacing.unit,
  },
  buttonImage:{
    position: 'absolute',
    left: 140,
    top: 120,
  },
  inputFile: {
    display: 'none',
  },
  
  inputCreateService:{
    outline:'none', 
    width: "100%",
    border:'1px solid #E0E0E0', 
    height:'52px', 
    paddingLeft:'24px', 
    paddingRight: '8px',
    paddingTop:'2px', 
    borderRadius:'3px',
    display: "flex",
    alignItems: "center",
    marginTop: 15
},
menu: {
    width: 200,
},
colorSwitchBase: {
    color: blue[300],
    '&$colorChecked': {
      color: blue[500],
      '& + $colorBar': {
        backgroundColor: blue[500],
      },
    },
  },
  colorBar: {},
  colorChecked: {},
  iOSSwitchBase: {
    '&$iOSChecked': {
      color: theme.palette.common.white,
      '& + $iOSBar': {
        backgroundColor: '#52d869',
      },
    },
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.sharp,
    }),
  },
  iOSChecked: {
    transform: 'translateX(15px)',
    '& + $iOSBar': {
      opacity: 1,
      border: 'none',
    },
  },
  iOSBar: {
    borderRadius: 13,
    width: 42,
    height: 26,
    marginTop: -13,
    marginLeft: -21,
    border: 'solid 1px',
    borderColor: theme.palette.grey[400],
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  iOSIcon: {
    width: 24,
    height: 24,
  },
  iOSIconChecked: {
    boxShadow: theme.shadows[1],
  },
});

const redirects = [
    {
      value: 1,
      label: 'Producto',
    },
    {
      value: 2,
      label: 'Categoría',
    },
    {
      value: 3,
      label: 'URL / Página web',
    },
  ];

class CardCarousel extends Component{
    state = { expanded: false, title: "", openConfirm: false, isProductSelected: false, redirect_id: 0, redirect_url: "" };
    componentDidMount(){
        this.renderProps(this.props);
    //   getCategories(data => {
    //     let _data = data.val();
    //     let _dataS = [{
    //         id: 0,
    //         name: 'Seleccione un Producto'
    //     }];
    //     _data.forEach(element => {
    //         element.products.forEach(product => {
    //             _dataS.push({
    //                 id: product.reference_id,
    //                 name: product.name
    //             });
    //         });
    //     });
    //     this.setState({productos: _dataS});
    //   });
    }
    renderProps = (props) => {
        const {title, redirectValue, element } = props;
        console.log("element", element);
        let _title = (title=='')?'Sin título':title;
        let oldState = {
            title: _title, editTitle: _title, redirectValue, isProductSelected: Boolean(element.redirect),
            redirect_url: element.redirect_url,
            redirect_id: (element.data)?element.data.reference_id: 0
        };
        this.setState({oldState, ...oldState});
    }
    componentWillReceiveProps(props){
        this.renderProps(props);
    }
    changeTitle = (event) => {
        this.setState({editTitle: event.target.value});
    }
    handleExpandClick = () => {
      this.setState(state => ({ expanded: !state.expanded, temporalImg: "", ...state.oldState }));
    };
    handleImagen = (event) => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({temporalImg: e.target.result});
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    }
    render(){
        const { expanded, title, editTitle, redirectValue, temporalImg, openConfirm, isProductSelected, redirect_id, redirect_url } = this.state;
        const { classes, theme, dateText, imagenUrl, handleDelete, productos } = this.props;
        
        return (
          <Grow in={true} timeout={600} >
            <Card className={classes.card} elevation={1}>
                <div style={{display: 'flex'}}>
                <CardMedia
                    className={classes.cover}
                    image={temporalImg||imagenUrl}
                    title={title}
                />
                {expanded?(
                    <div className={classes.buttonImage}>
                        <input onChange={this.handleImagen.bind(this)} accept="image/*" className={classes.inputFile} id="icon-button-file-image" type="file" />
                        <label htmlFor="icon-button-file-image">
                            <Button variant="contained" color="primary" className={classes.button} component="span">
                                <Icon>photo_camera</Icon>
                            </Button>
                        </label>
                    </div>
                ):<div></div>}
                <CardHeader
                    className={classes.header}
                    action={
                        <div>
                            <IconButton disabled={expanded} aria-label="editar" onClick={this.handleExpandClick}>
                                <Icon>edit</Icon>
                            </IconButton>
                            <IconButton aria-label="borrar" onClick={handleDelete}>
                                <Icon>delete</Icon>
                            </IconButton>
                        </div>
                    }
                    title={expanded?(
                        <TextField
                        type="text"
                        typeStyle="outlined"
                        label="Título"
                        value={editTitle}
                        onChange={this.changeTitle} />
                    ):title}
                    subheader={expanded?"":moment(dateText).format('MMMM DD, YYYY')}
                    />
                </div>
                <Collapse in={expanded} timeout="auto" unmountOnExit className={classes.collapse} >
                  <CardContent style={{display: 'flex', alignContent: 'space-evenly', alignItems: 'center', padding: 12}}>
                    <div>
                        <FormGroup row>
                            <FormControlLabel
                            control={
                                <Switch
                                classes={{
                                    switchBase: classes.iOSSwitchBase,
                                    bar: classes.iOSBar,
                                    icon: classes.iOSIcon,
                                    iconChecked: classes.iOSIconChecked,
                                    checked: classes.iOSChecked,
                                }}
                                checked={isProductSelected}
                                onChange={() => {
                                    this.setState({isProductSelected: !isProductSelected});
                                }}
                                value="isProductSelected"/>
                            }
                            label="URL | Producto"
                            /> 
                        </FormGroup>
                    </div>
                    <div style={{flex: 1, marginRight: 35}}>
                        {(!isProductSelected)?<div>
                            <TextField
                                type="text"
                                typeStyle="outlined"
                                id="url"
                                label="Ingrese una URL"
                                value={redirect_url}
                                onChange={this.handleChange('redirect_url')}
                                className={classes.inputStyle}
                            />
                        </div>:<div>
                        <Select
                            value={redirect_id}
                            onChange={this.handleChange('redirect_id')}
                            className={classes.inputCreateService}
                            name="product"
                        >
                            {productos.map((prop, key) => {console.log(prop.id); return(
                                <MenuItem key={key} value={prop.id}>
                                {prop.name}
                                </MenuItem>
                            )})}
                        </Select>
                        </div>}
                        
                            
                    </div>
                    <div>
                    <Button size="medium" color="primary" className={classes.button}>
                        Actualizar
                    </Button>
                    <Button variant="contained" size="medium" color="primary" className={classes.button} onClick={this.handleExpandClick}>
                        Cancelar
                    </Button>
                    </div>
                  </CardContent>
                </Collapse>
            </Card>
          </Grow>
        );
      }
}


CardCarousel.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(CardCarousel);