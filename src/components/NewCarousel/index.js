import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FullScreenDialog from '../FullScreenDialog';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {TextField} from '../Material';
import Files from 'react-files';
import blue from '@material-ui/core/colors/blue';

import TextFieldMaterial from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { setCarousels, getCarousels, getCategories, uploadFiles, setImagCarousel } from '../../firebase/Database';
import { Select, Divider, Switch, FormGroup, FormControlLabel, Paper, Icon } from '@material-ui/core';

const styles = theme => ({
    container: {
        padding: "40px 13%",
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    inputStyle: {
        // margin: "10px 15px",
        width: "100%",
    },
    inputCreateService:{
        outline:'none', 
        width: "100%",
        border:'1px solid #E0E0E0', 
        height:'52px', 
        paddingLeft:'24px', 
        paddingRight: '8px',
        paddingTop:'2px', 
        borderRadius:'3px',
        display: "flex",
        alignItems: "center",
        marginTop: 15
    },
    menu: {
        width: 200,
    },
    button: {
        backgroundColor: "#FFC107",
        color: "#FFF",
        margin: "20px 10px",
    },
    colorSwitchBase: {
        color: blue[300],
        '&$colorChecked': {
          color: blue[500],
          '& + $colorBar': {
            backgroundColor: blue[500],
          },
        },
      },
      colorBar: {},
      colorChecked: {},
      iOSSwitchBase: {
        '&$iOSChecked': {
          color: theme.palette.common.white,
          '& + $iOSBar': {
            backgroundColor: '#52d869',
          },
        },
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
          easing: theme.transitions.easing.sharp,
        }),
      },
      iOSChecked: {
        transform: 'translateX(15px)',
        '& + $iOSBar': {
          opacity: 1,
          border: 'none',
        },
      },
      iOSBar: {
        borderRadius: 13,
        width: 42,
        height: 26,
        marginTop: -13,
        marginLeft: -21,
        border: 'solid 1px',
        borderColor: theme.palette.grey[400],
        backgroundColor: theme.palette.grey[50],
        opacity: 1,
        transition: theme.transitions.create(['background-color', 'border']),
      },
      iOSIcon: {
        width: 24,
        height: 24,
      },
      iOSIconChecked: {
        boxShadow: theme.shadows[1],
      },
});

class NewNotification extends React.Component {
    state = {
        disabledAddC: true,
        new_carousel: "",
        layout: 0,
        carouseles: [],
        productos: [],
        product: 0,
        isProductSelected: false,
        picture: [],
        url: "",
    };    
    componentDidMount(){
        getCarousels(data => {
            let _data = [{
                id: 0,
                name: 'Seleccione un Carousel'
            }];
            let carousel = data.val();
            for(let key in carousel){
                _data.push({
                    id: key,
                    name: carousel[key].name
                });
            }
          this.setState({carouseles: _data});
      });
      getCategories(data => {
        let _data = data.val();
        let _dataS = [{
            id: 0,
            name: 'Seleccione un Producto'
        }];
        _data.forEach(element => {
            element.products.forEach(product => {
                _dataS.push({
                    id: JSON.stringify(product),
                    name: product.name
                });
            });
        });
        this.setState({productos: _dataS});
      });
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        }, ()=>{
            if(this.state.new_carousel !== "") this.setState({disabledAddC: false});
            else this.setState({disabledAddC: true});
        });
    };  
    handleSendCarousel = ()=>{
        console.log((new Date()));
        setCarousels({
            createdDate: (new Date()),
            identifier: this.state.new_carousel,
            images: [],
            layout: 0,
            name: this.state.new_carousel,
            status: 1
        }, ()=>{
            this.setState({
                disabledAddC: true,
                new_carousel: ""
            });
        });
    }
    onFilesError = (error, file) => {
        console.log('error code ' + error.code + ': ' + error.message)
    }
    onFilesChange = (files) => {
        this.setState({picture: files}, ()=>{
            console.log(files[0]);
        });
    }
    sendImageCarousel = ()=>{
        const {picture, layout, isProductSelected, product, url} = this.state;
        console.log(layout, isProductSelected, product, url);
        if(picture.length>0){
            if(layout!==0&&(product!==""||url!=="")){
                uploadFiles(picture[0], (imagenUrl) => {
                    setImagCarousel({
                        createdDate: (new Date),
                        redirect: Number(isProductSelected),
                        data: isProductSelected?JSON.parse(product):[],
                        redirect_url: isProductSelected?"":url,
                        status: 1,
                        subtitle: "",
                        title: "",
                        text: "",
                        url: imagenUrl
                    }, layout, ()=>{
                        this.props.handleClose();
                    })
                });
            }
        }
    }
    render(){
        const {classes, open, handleClose} = this.props;
        const {disabledAddC, new_carousel, layout, carouseles, isProductSelected, productos, product, url} = this.state;
        return(
            <div>
                <FullScreenDialog toolBarColor={"#FFC107"} title={"Nuevo Carousel"} open={open} handleClose={handleClose} content={
                    <div className={classes.container}>
                        <Grid container spacing={24}>
                            <Grid item xs={9}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="new_carousel"
                                    label="Nuevo Carousel"
                                    value={new_carousel}
                                    onChange={this.handleChange('new_carousel')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={3}>
                                <Button disabled={disabledAddC} onClick={this.handleSendCarousel} variant="contained" color={"primary"} style={{height: 50, margin: '17px 0'}}>
                                    AÑADIR CAROUSEL
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <br /><Divider /><br />
                        </Grid>
                        <Grid container xs={12} spacing={24}>
                                <Grid item xs={8}>
                                    <Select
                                        value={layout}
                                        onChange={this.handleChange('layout')}
                                        className={classes.inputCreateService}
                                        name="layout"
                                    >
                                        {carouseles.map((prop, key) => (
                                            <MenuItem key={key} value={prop.id}>
                                            {prop.name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </Grid>
                                <Grid item xs={4}>
                                    <FormGroup row style={{
                                        marginLeft: 40,
                                        marginTop: 20
                                    }}>
                                        <FormControlLabel
                                        control={
                                            <Switch
                                            classes={{
                                                switchBase: classes.iOSSwitchBase,
                                                bar: classes.iOSBar,
                                                icon: classes.iOSIcon,
                                                iconChecked: classes.iOSIconChecked,
                                                checked: classes.iOSChecked,
                                              }}
                                            checked={isProductSelected}
                                            onChange={() => {
                                                this.setState({isProductSelected: !isProductSelected});
                                            }}
                                            value="isProductSelected"/>
                                        }
                                        label="URL | Producto"
                                        /> 
                                    </FormGroup>
                                </Grid>
                                {(!isProductSelected)?<Grid item xs={12}>
                                    <TextField
                                        type="text"
                                        typeStyle="outlined"
                                        id="url"
                                        label="Ingrese una URL"
                                        value={url}
                                        onChange={this.handleChange('url')}
                                        className={classes.inputStyle}
                                    />
                                </Grid>:<Grid item xs={12}>
                                    <Select
                                        value={product}
                                        onChange={this.handleChange('product')}
                                        className={classes.inputCreateService}
                                        name="product"
                                    >
                                        {productos.map((prop, key) => (
                                            <MenuItem key={key} value={prop.id}>
                                            {prop.name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </Grid>}
                                <Grid item xs={6}>
                                <div style={{ width:'212px', marginRight:'24px', display:'inline-block' }}>
                                    <p style={{ marginBottom:'26px' }}>Seleccione una Imagen</p>
                                    <Files
                                        ref='pictures'
                                        className='files-dropzone-list'
                                        style={(!(this.state.picture.length>0))? {height: '74px', width:'74px', display: 'inline-block',}:{height:'0', width:'0'} }
                                        onChange={this.onFilesChange}
                                        maxFiles={1}
                                        onError={this.onFilesError}
                                        accepts={['image/*']}
                                        maxFileSize={10000000}
                                        minFileSize={0}
                                        clickable
                                    >
                                        {(!(this.state.picture.length>0))?
                                            <Paper style={{ height: 74,
                                                    width: 80,
                                                    padding: 15,
                                                    paddingTop:'23px',
                                                    textAlign: 'center',
                                                    display: 'inline-block',
                                                    backgroundColor:'#e6e6e6',
                                                    cursor:'pointer'
                                                }}
                                                zDepth={0}
                                            >   
                                                <Icon style={{ width:'30px', height:'30px', color: "#4a4a4a", fontSize: 30  }} color={'#4a4a4a'}>view_carousel</Icon><br /><br />
                                                <p style={{ fontSize:'14px', color:'rgba(0,0,0,0.84)', marginTop:'-5px'}}>Agregar al Carousel</p>
                                            </Paper>:''}
                                    </Files>
                                        {/* <button onClick={this.filesRemoveAll}>Borrar todo</button> */}
                                        {/* <button onClick={this.filesUpload}>Upload</button> */}
                                    {this.state.picture.length > 0 ? 
                                        <div className='files-list'>
                                            <ul style={{ paddingLeft:'0', display:'inline-block', marginBottom:'-15px' }}>{this.state.picture.map((file) =>
                                            <li className='files-list-item' key={file.id} style={{ 
                                                    listStyleType:'none', 
                                                    display:'inline-block', 
                                                    float:'left', 
                                                    marginRight:'12px',
                                                    position:'relative'
                                                }}>
                                                <div className='files-list-item-preview'>
                                                {file.preview.type === 'image'
                                                ? <img className='files-list-item-preview-image' src={file.preview.url} style={{ width:'74px', height:'74px' }}/>
                                                : <div className='files-list-item-preview-extension'>{file.extension}</div>}
                                                </div>
                                                <div
                                                id={file.id}
                                                className='files-list-item-remove'
                                                onClick={()=>this.setState({picture: []})} // eslint-disable-line
                                                ><p style={{ cursor:'pointer', color:'red', fontSize:'12px', bottom:'72%', position:'absolute', left:'86%', backgroundColor:'#ff5722', borderRadius:'50%', padding:'1px 6px', paddingTop:'3px', color:'white' }}>X</p></div>
                                            </li>
                                            )}</ul>
                                        </div>
                                        : null
                                    }
                                </div>
                            </Grid>
                            <Grid item xs={6} style={{
                                display: "flex",
                                justifyContent: "space-evenly",
                                alignItems: "flex-end"
                            }}>
                                <Button onClick={this.sendImageCarousel} variant="contained" color={"primary"}  style={{height: 50}}>
                                    AÑADIR IMAGEN AL CAROUSEL
                                </Button>
                                <Button onClick={handleClose} variant="contained"  style={{height: 50}}>
                                    CANCELAR
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                } />
            </div>
        );
    }
}

NewNotification.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewNotification);