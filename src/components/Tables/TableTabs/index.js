import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import SwipeableViews from 'react-swipeable-views';
import Avatar from '@material-ui/core/Avatar';
import Tab from '@material-ui/core/Tab';
import { Scrollbars } from 'react-custom-scrollbars';

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
      },
    appBar: {
        transition: '.1s',
        boxShadow: 'none'
    },
    flex: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    tabBadge: {
        color: theme.palette.primary.main,
        backgroundColor: theme.palette.primary.contrastText,
        margin: '3px 10px',
        width: 24,
        height: 24,
        fontSize: 10
    },
    tabBadgeBigData: {
        borderRadius: "12px",
        padding: "0px 11px"
    }
});

class TableTabs extends Component {
    constructor(props){
        super(props);
        this.searchRef = React.createRef();
        this.state = {
            valueTabsIndex: 0
        }
        this.handleChangeIndex = this.handleChangeIndex.bind(this);
    }
    componentDidMount(){
       
    }
    handleChange = (event, valueTabsIndex) => {
        this.setState({ valueTabsIndex });
    };
    renderLabel = (props) => {
        const {classes} = this.props;
        return(
            <div className={classes.flex}>
                <Avatar className={classNames(classes.tabBadge, {
                    [classes.tabBadgeBigData]: String(props.value).length>2
                })}>{props.value}</Avatar>
                {props.name}
            </div>
        );
    }
    handleChangeIndex = index => {
        this.setState({ valueTabsIndex: index });
    };
    render() {
        const {classes, theme, tabs, fullWidth, scrollable} = this.props;
        const {valueTabsIndex } = this.state;
        return (
            <div>
                <AppBar position="static" color="primary" className={classes.appBar}>
                        <Tabs
                            value={valueTabsIndex}
                            onChange={(this.handleChange)}
                            indicatorColor="inherit"
                            textColor="inherit"
                            // scrollable
                            // scrollButtons="auto" 
                            fullWidth>
                            {tabs.map((element, key)=>
                                <Tab disabled={element.disabled} key={key} label={this.renderLabel({
                                    name: element.title,
                                    value: element.count
                                })} />
                            )}
                    </Tabs> 
                </AppBar>
                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={valueTabsIndex}
                    onChangeIndex={this.handleChangeIndex} >
                    {tabs.map((element, key)=>
                    // style={{height:"80vh"}}
                        <div key={key} style={{overflow: "hidden"}} dir={theme.direction}>
                            <Scrollbars style={{height: "100vh"}} autoHide autoHideTimeout={1500} thumbMinSize={40} autoHideDuration={200} > 
                                {element.content}
                            </Scrollbars>
                        </div>
                    )}
                </SwipeableViews>
            </div>
        );
    }
  }
  
  TableTabs.propTypes = {
    //scrollable: PropTypes.bool,
    fullWidth: PropTypes.bool,
    tabs: PropTypes.array.isRequired
  };

export default withStyles(styles, { withTheme: true })(TableTabs);