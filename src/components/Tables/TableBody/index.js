import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

const styles = theme => ({
 
});

let counter = 0;
function createData(name, calories, fat, carbs, protein) {
  counter += 1;
  return { id: counter, name, calories, fat, carbs, protein };
}

function getSorting(order, orderBy) {
  return order === 'desc'
    ? (a, b) => (b[orderBy] < a[orderBy] ? -1 : 1)
    : (a, b) => (a[orderBy] < b[orderBy] ? -1 : 1);
}

class TableBodyComponent extends Component {
    static defaultProps = {
      onClickColumn: ()=>{}
    }
    render() {
      const { classes, onClickColumn, columnDataHeader, data, emptyRows, handleClick, order, orderBy, selected, rowsPerPage, page, noCheck } = this.props;
      return (
            <TableBody>
              {data.map((n, i) => (
                    <TableRow
                      hover
                      //onClick={event => handleClick(event, n.id)}
                      role="checkbox"
                      aria-checked={i}
                      tabIndex={-1}
                      key={i}
                      selected={false} >
                      {(noCheck||
                      <TableCell padding="checkbox" style={{minWidth: 56, textAlign: 'center'}}>
                        <Checkbox checked={false} />
                      </TableCell>)}
                      {n.map((l, f) => (
                        <TableCell 
                          onClick={()=>onClickColumn(n)}
                          padding={columnDataHeader[f].disablePadding ? 'none' : 'default'}
                          style={columnDataHeader[f].styles}
                          key={f}
                          numeric>
                          {l}
                        </TableCell>
                      ))}
                    </TableRow>
                  ))}
                {/* <div style={{ height: 80 }}></div> */}
            </TableBody>
      );
    }
  }
  
  TableBodyComponent.propTypes = {
      
  };

export default withStyles(styles)(TableBodyComponent);


// <TableBody>
// {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
//   return (
//     <TableRow key={n.id}>
//       <TableCell component="th" scope="row">
//         {n.name}
//       </TableCell>
//       <TableCell numeric>{n.calories}</TableCell>
//       <TableCell numeric>{n.fat}</TableCell>
//     </TableRow>
//   );
// })}
// {emptyRows > 0 && (
//   <TableRow style={{ height: 48 * emptyRows }}>
//     <TableCell colSpan={6} />
//   </TableRow>
// )}
// </TableBody>