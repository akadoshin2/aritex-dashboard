import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Icon, IconButton } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import Close from '@material-ui/icons/Close';
import Input from '@material-ui/core/Input';

const styles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    toolbar: {
        transition: '.1s',
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.primary.contrastText
    },
    title: {
        flex: '0 0 auto',
    },
    iconTitle: {
        marginRight: 18
    },
    spacer: {
        flex: '1 1 100%',
    },
    search: {
        transition: '.3s',
        width: 0,
        marginRight: "-6px",
        color: theme.palette.primary.contrastText,
        margin: theme.spacing.unit,
        borderBottomColor: theme.palette.primary.main,
        '&:after': {
            borderBottomColor: theme.palette.primary.contrastText,
        },
    },
    searchActive: {
        width: "100%"
    }
});

class TableToolbarComponent extends Component {
    constructor(props){
        super(props);
        this.searchRef = React.createRef();
        this.state = {
            showSearchBar: false, 
            searchValue: ""
        }
        this.showSearchBarFnc = this.showSearchBarFnc.bind(this);
    }
    componentDidMount(){
        console.log(this.searchRef);
        this.searchRef.current.onblur = ()=>{
            if(this.state.searchValue==""){
                this.setState({showSearchBar: false, searchValue: ""});
            }
        }
    }
    showSearchBarFnc(){
        if(this.props.searchAction){
            this.setState({showSearchBar: !this.state.showSearchBar, searchValue:""}, (state)=>{
                this.searchRef.current.focus();
                this.props.searchAction(this.state.searchValue, this.state.showSearchBar);
            });
        }
    }
    searchAction = event =>{
        if(this.props.searchAction){
            this.setState({searchValue: event.target.value});
            this.props.searchAction(this.state.searchValue, this.state.showSearchBar);
        }
    }
    render(){
        const { classes, title, iconName } = this.props;
        const { showSearchBar, searchValue } = this.state;
        return (
            <Toolbar className={classes.toolbar}>
                <Icon color="inherit" className={classes.iconTitle}>{iconName}</Icon>
                <Typography variant="title" color="inherit">
                    {title}
                </Typography>
                <div className={classes.spacer} />
                    <IconButton style={{marginRight: "-6px"}} aria-label="Buscar" color="inherit" onClick={this.showSearchBarFnc}>
                        {showSearchBar?<Close />:<Search />}
                    </IconButton>
                    <Input
                        placeholder={`Buscar en ${title}`}
                        inputRef={this.searchRef}
                        value={searchValue}
                        onKeyUp={(event)=>this.searchAction(event)}
                        onChange={(event)=>this.searchAction(event)}
                        className={classNames(classes.search, {
                            [classes.searchActive]: showSearchBar,
                        })}
                        inputProps={{
                            'aria-label': 'Buscar',
                        }}
                    />
            </Toolbar>
        );
    }
}
  
TableToolbarComponent.propTypes = {
    title: PropTypes.string.isRequired,
    iconName: PropTypes.string.isRequired
};
  
export default withStyles(styles)(TableToolbarComponent);