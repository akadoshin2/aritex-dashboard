import TableHead from './TableHead';
//import Table from '@material-ui/core/Table';
import TableToolbar from './TableToolbar';
import TableTabs from './TableTabs';
import TableBody from './TableBody';
import Table from './Table';

export {
    Table,
    TableToolbar,
    TableHead,
    TableTabs,
    TableBody
}