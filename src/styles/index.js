import styled from 'styled-components';

// Shadow before Background Imagen
export const BeforeBackgroundImagen = styled.div`
    border-radius: ${props=>props.radius||'0px'};
    display: block;
    padding: 0;
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    background-color: #0000001a;
    box-shadow: inset 0 0 70px 0px #000000a1;
`;

// Background Imagen
export const BackgroundImagen = styled.div`
    border-radius: ${props=>props.radius||'0px'};
    background-color: #E0E0E0;
    background-image: url(${props=>props.url});
    width: ${props=>props.width||'100%'}; height: ${props=>props.height||'100%'};
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
`;

export const ContainerClear = styled.span`display: contents;`;
export const CenterDiv = styled.span`text-align: center; display: block;`;
export const PositionRelative = styled.div`position: relative`;
export const ForgotPass = styled.a`
    margin: 6px 40px 50px;
    height: 16px;
    color: #757575;
    font-family: Roboto;
    font-size: 14px;
    font-weight: 300;
    line-height: 16px;
    text-align: center;
    display: block;
    text-decoration: none;
    outline: none;
    cursor: pointer;
`;
export const VersionCardLogin = styled.p`
    position: absolute;
    right: 10px;
    bottom: 0;
    width: 23px;
    color: #9B9B9B;
    font-family: Roboto;
    font-size: 11px;
    font-weight: 300;
    line-height: 13px;
`;
export const MaterialIcons = styled.i`
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;
  line-height: 1;
  letter-spacing: normal;
  text-transform: none;
  display: inline-block;
  white-space: nowrap;
  word-wrap: normal;
  direction: ltr;
  -webkit-font-feature-settings: 'liga';
  -webkit-font-smoothing: antialiased;
`;