export default [
    {
        name: "LOGIN",
        path: "/login",
        enabled: true,
        private: false,
        isRoute: true,
        onlyNoAuth: true,
        component: 'Login'
    },
    {
        name: "HOME",
        path: "/",
        label: "Dashboard",
        icon: "signal_cellular_alt",
        enabled: true,
        private: true,
        isRoute: true,
        isSidebar: true,
        component: 'Main'
    },
    {
        name: "CATEGORY",
        path: "/categorias",
        label: "Categorías",
        icon: "collections_bookmark",
        isRoute: true,
        private: true,
        enabled: true,
        isSidebar: true,
        component: 'Categories'
    },
    {
        name: "ORDERS",
        path: "/pedidos",
        label: "Pedidos",
        icon: "assignment",
        isRoute: true,
        private: true,
        enabled: true,
        isSidebar: true,
        component: 'Pedidos'
    },
    {
        name: "CAROUSEL",
        path: "/carouseles",
        label: "Carouseles",
        icon: "view_carousel",
        isRoute: true,
        private: true,
        enabled: true,
        isSidebar: true,
        component: 'Carousels'
    },
    {
        name: "USERS",
        path: "/usuarios",
        label: "Usuarios",
        icon: "account_circle",
        isRoute: true,
        private: true,
        enabled: true,
        isSidebar: true,
        component: 'Users'
    },
    {
        name: "NOTIFICATIONS",
        path: "/notificaciones",
        label: "Notificaciones",
        icon: "notifications",
        isRoute: true,
        private: true,
        enabled: true,
        isSidebar: true,
        component: 'Notifications'
    },
];