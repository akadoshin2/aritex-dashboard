import {database, storage, admin} from '../firebase';
import AWS from 'aws-sdk';
// const Bucket = 'aritex-prod';
const Bucket = 'aritex-test';
AWS.config.update({ accessKeyId: 'AKIAIHN7ALEDRLEWSNFA', secretAccessKey: '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', region: 'us-east-2' });
const S3 = new AWS.S3({params: {Bucket: Bucket}});

//References
export const ReferencesName = {
    theme: "/ThemeApp/palette",
    carousels: "/ThemeApp/carousels",
    categories: "/ThemeApp/data/products",
    clientes: "/ThemeApp/data/clientes",
    bestCategories: "/ThemeApp/data/bestCategories",
    users: "/ThemeApp/users",
    notifications: "/ThemeApp/data/notifications",
    pedidos: "/ThemeApp/data/orders"
};

export const getThemeApp = () => 
    database.ref(ReferencesName.theme).once('value').then(snapshot=>{
        console.log(snapshot.val());
        return snapshot.val();
    });

export const getCarousels = (callback) => database.ref(ReferencesName.carousels).on('value', callback);
export const setCarousels = (carousel, callback = null) => database.ref(ReferencesName.carousels).push(carousel, callback);
export const updateAllCarousels = (carousel, callback = null) => database.ref(ReferencesName.carousels).set(carousel, callback);
export const setImagCarousel = (imgs, parentId, callback = null) => database.ref(ReferencesName.carousels).child(parentId).child('images').push(imgs, callback);
export const deleteCategoriesImg = (catParentId, idImg, callback = null) => database.ref(`${ReferencesName.carousels}/${catParentId}/images/${idImg}`).set(null, callback);
export const deleteCategories = (catParentId, callback = null) => database.ref(ReferencesName.carousels).child(catParentId).set(null, callback);
export const uploadFiles = (file, callback = null) => {
    // let upload = new AWS.S3();
    // var fd = new FormData();

    let ref = storage.child("attatchments/" + (new Date).getTime() + '-' + file.name);
    ref.put(file).then(function(snapshot) {
        ref.getDownloadURL().then((url) => {
            callback(url);
        });
    });

    // var params = {
    //     Key: key,
    //     ContentType: file.type,
    //     Body: file,
    //     ACL: 'public-read'
    // };

    // S3.putObject(params, callback); // err, data
}


export const getCategories = (callback) => database.ref(ReferencesName.categories).on('value', callback);
export const getBestCategories = (callback) => database.ref(ReferencesName.bestCategories).on('value', callback);
export const setCategories = (child, categories, callback = null) => database.ref(`${ReferencesName.categories}`).child(child).set(categories, callback);

export const getUsers = (callback) => database.ref(ReferencesName.users).on('value', callback);
export const setUser = (user, callback = null) => database.ref(ReferencesName.users).child(user.uid).set(user, callback);
export const deleteUser = (userId, callback = null) => {
    admin.auth().deleteUser(userId).then(function() {
            console.log("Successfully deleted user");
            database.ref(ReferencesName.users).child(userId).set(null, callback);
        })
        .catch(function(error) {
            console.log("Error deleting user:", error);
        });
}


export const getClients = (callback) => database.ref(ReferencesName.clientes).on('value', callback);


export const getNotifications = (callback) => database.ref(ReferencesName.notifications).on('value', callback);
export const setNotifications = (notification, callback = null) => database.ref(ReferencesName.notifications).push(notification, callback);
export const deleteNotification = (notificationId, callback = null) => database.ref(ReferencesName.notifications).child(notificationId).set(null, callback);

export const getPedidos = (callback) => database.ref(ReferencesName.pedidos).on('value', callback);
export const setPedidos = (pedido, data, callback = null) => database.ref(`${ReferencesName.pedidos}`).child(pedido).set(data, callback);
