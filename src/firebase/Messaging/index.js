import http from 'http';
import { setNotifications } from '../Database';

const post_options = {
    host: 'fcm.googleapis.com',
    port: '80',
    path: '/fcm/send',
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'key=AIzaSyAPL4ggLWh52D5P0uqX4m_2TPBpmbdVFHI'
    }
  };

export const sendNotifications = ({title, body, to, ticker, big_text, sub_text, segmentation, draft} = {to: "notificaciones"}) => {
    let toSend = "";
    switch (to) {
      case "asesores":
        toSend = "ASESORES";
        break;
      case "clientes":
        toSend = "CLIENTES";
        break;
      default:
        toSend = "TODOS";
        break;
    }  
    if(draft){
      setNotifications({
          audience: toSend,
          date_create: new Date().toString(),
          draft: draft||false,
          message: body
      }, resNoti=>{
          console.log(resNoti);
      });
      return 0;
    }
    let post_data = {
        "to" : "/topics/"+to,
        "notification": {
          "title": title || 'Aritex',
          "body":  body || 'Mensaje de Aritex'
        },
        "data" : {
          "custom_notification": {
            "body": body || 'Mensaje de Aritex',
            "title": title || 'Aritex',
            "priority": "high",
            "icon":"ic_notif",
            "group": "GROUP",
            "show_in_foreground": true,
            "ticker": ticker || 'Notificación de Aritex',
            "lights": true,
            "big_text": big_text || 'Notificación',
            "sub_text": sub_text || 'Aritex',
            "segmentation": segmentation || 3
          }
        }
      };
      console.clear();
    let post_req = http.request(post_options, response => {
        response.setEncoding('utf8');
        response.on('data', function (chunk) {
          try {
            let resp = JSON.parse(chunk);
              setNotifications({
                  id: resp.message_id,
                  audience: toSend,
                  date_create: new Date().toString(),
                  draft: draft||false,
                  message: post_data.notification.body
              }, resNoti=>{
                  console.log(resNoti);
              });
            return ({ status: 200, message: 'Push sended' });
          } catch (err) {
            return ({ status: 500, message: 'Error from the server call' });
          }
        });
      });
    post_req.write(JSON.stringify(post_data));
    post_req.end();
};

