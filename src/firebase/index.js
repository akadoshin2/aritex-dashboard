import * as auth from './Auth';
import * as database from './Database';
import * as firebase from './firebase';

export {
  auth,
  database,
  firebase,
};