import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';
import * as admin from 'firebase-admin';
import serviceAccount from './keyFirebase.json';

  var config = {
    apiKey: "AIzaSyDIGxDx4xJPlanUapazmBlz109RxvPB560",
    authDomain: "reacttestaritex.firebaseapp.com",
    databaseURL: "https://reacttestaritex.firebaseio.com",
    projectId: "reacttestaritex",
    storageBucket: "reacttestaritex.appspot.com",
    messagingSenderId: "181436959165"
  };

firebase.initializeApp(config);
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://reacttestaritex.firebaseio.com'
});

const auth = firebase.auth();
const database = firebase.database();
const storage = firebase.storage().ref();

export {
    auth,
    database,
    storage,
    admin
};