import {auth} from '../firebase';

// Sign Up
export const doCreateUserWithToken = (id, {admin = false, user = false, internal = false, client = false} = {})=> 
    auth.createCustomToken(id, {admin, user, internal, client});

export const doCreateUserWithEmail = (email, password)=>
    auth.createUserWithEmailAndPassword(email, password);

// Sign In
export const doSignInWithToken = (token)=> 
    auth.signInWithCustomToken(token);

export const doSignInWithEmail = (email, password)=>
    auth.signInWithEmailAndPassword(email, password);

// Sign out
export const doSignOut = ()=>
    auth.signOut();

// Password reset
export const doPasswordReset = (email)=>
    auth.sendPasswordResetEmail(email);

// Password Change
export const doPasswordUpdate = (password)=>
    auth.currentUser.updatePassword(password);