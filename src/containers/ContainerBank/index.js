import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Statistics from '../../components/Statistics';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

class Bank extends Component {
    constructor(props){
      super(props);
    }
    render() {
      const { classes } = this.props;
      return (
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Grid
              container
              spacing={0}
              className={classes.demo}
              alignItems={"stretch"}
              direction={"column"}
              justify={"flex-start"} >
                <Grid item>
                  <Statistics />
                </Grid>
            </Grid>
          </Grid>
        </Grid>
      );
    }
}

Bank.propTypes = {
    
}

export default withStyles(styles)(Bank);