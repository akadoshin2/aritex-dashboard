import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {TextField, Button} from '../../../components/Material';
import { auth } from '../../../firebase';
import ForgetPassword from '../ForgetPassword';

const styles = theme => ({
    inputStyle: {
        width: "75%"
    }
});

const initialState = {username: '', password: '', loading: false};

class FormControl extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(name, event) {
        this.setState({[name]: event.target.value});
    }
    handleSubmit(event) {
        const {username, password} = this.state;
        if(username!==""&&password!==""){
            this.setState({loading: true}, ()=>{
                auth.doSignInWithEmail(this.state.username, this.state.password).then(response=>{
                    this.setState(initialState);
                }).catch(error=>{
                    console.log(error);
                    this.setState({loading: false});
                })
            });
        }
        event.preventDefault();
    }
    render(){
        const { loading, password, username } = this.state;
        const { classes } = this.props;
        return(
            <form onSubmit={this.handleSubmit}>
                <TextField 
                    type="email"
                    icon={"account_circle"}
                    typeStyle="outlined"
                    label="Username"
                    value={username}
                    onChange={(e)=> this.handleChange("username", e)}
                    className={classes.inputStyle}/>

                <TextField
                    type="password"
                    icon={"lock"}
                    typeStyle="outlined"
                    label="Password"
                    value={password}
                    containerStyles={{
                        marginTop: 7,
                    }}
                    onChange={(e)=> this.handleChange("password", e)}
                    className={classes.inputStyle} />
                    <ForgetPassword email={username} />
                    <Button type="submit" variant="contained" color="primary" loading={loading}>
                        Ingresar
                    </Button>
            </form>
        );
    }
}

export default withStyles(styles)(FormControl);