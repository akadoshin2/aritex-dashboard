import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Img from '../../components/Img';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {BackgroundImagen, BeforeBackgroundImagen, CenterDiv, VersionCardLogin} from '../../styles';
import FormControl from './FormControl';

const styles = theme => ({
    root: {
      flexGrow: 1,
      height: '100vh',
    },
    flex: {
        height: 520
    },
    containerCard:{
        flexGrow: 1,
        height: 520,
        position: 'relative',
        borderRadius: 13,
        boxShadow: '0px 0px 50px 2px rgba(0, 0, 0, 0.1)'
    },
    titleCard: {
        padding: "70px 0 35px 0",
        textAlign: 'center'
    },
    titleCardH: {
        textTransform: "uppercase",
        fontWeight: 300,
        color: '#333333',
        fontSize: 36
    },
    subTitleCardH: {
        color: theme.palette.primary.light,
        fontWeight: 100,
        fontSize: 24
    },
    paper: {
      padding: theme.spacing.unit * 2,
      color: theme.palette.text.secondary,
    },
    control: {
      padding: theme.spacing.unit * 2,
    },
  });

class Login extends Component {
    render() {
      const { classes } = this.props;
      return (
        <Grid 
            container
            xs={12}
            className={classes.root}
            direction={'row'}
            alignItems={'center'}
            justify={'center'} >
                <Grid item xs={10} md={8}>
                    <Paper className={classes.containerCard} elevation={0}>
                        <Grid container direction={'row'} wrap="nowrap">
                            <Hidden only={['xs']}>
                                <Grid container md={6} className={classes.flex}>
                                    <BackgroundImagen radius={"15px 0 0 15px"} url={"/assets/banner_login.jpeg"}>
                                        <BeforeBackgroundImagen radius={"15px 0 0 15px"}>
                                            <Grid container 
                                                className={classes.flex}
                                                direction={'column'}
                                                alignItems={'center'}
                                                justify={'center'}>
                                                    {/* <Img width="42%" src="http://www.aritexonline.com.co/wp-content/uploads/2014/09/LOGO.png" /> */}
                                            </Grid>
                                        </BeforeBackgroundImagen>
                                    </BackgroundImagen>
                                </Grid>
                            </Hidden>
                            <Grid container xs={12} md={6}
                                    direction={'column'}
                                    alignItems={'stretch'}
                                    justify={'flex-start'}
                                    className={classes.flex}
                                    spacing={5} >
                                    <div className={classes.titleCard}>
                                        <Grid item>
                                            <Typography className={classes.titleCardH} variant={"display1"}>BIENVENIDO</Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography className={`${classes.titleCardH} ${classes.subTitleCardH}`} variant={"subheading"}>ARITEX WEB ADMIN</Typography>
                                        </Grid>
                                    </div>
                                    <Grid item>
                                        <CenterDiv><FormControl /></CenterDiv>
                                    </Grid>
                            </Grid>
                        </Grid>
                        <VersionCardLogin>v2.0</VersionCardLogin>
                    </Paper>
                </Grid> 
        </Grid>
      );
    }
}

export default withStyles(styles)(Login);