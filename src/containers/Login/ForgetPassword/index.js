import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {TextField, Button} from '../../../components/Material';
import {ForgotPass} from '../../../styles';
import { auth } from '../../../firebase';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme => {console.log(theme.palette.error.main); return({
    inputStyle: {
        width: "100%"
    },
    errorDialogText: {
        color: theme.palette.error.main
    }
})};

const initialState = {email: '', open: false, loading: false, error: false};

class ForgetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSend = this.handleSend.bind(this);
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    componentWillReceiveProps(nextProps){
        this.setState({email: nextProps.email});
    }
    handleChange(event) {
        this.setState({email: event.target.value});
    }
    handleSend() {
        const { email } = this.state;
        if(email!=''){
            this.setState({loading: true});
            auth.doPasswordReset(email).then(res=>{
                this.setState(initialState);
            }).catch(error=>{
                this.setState({loading: false, error: true});
            });
        }
    }
    handleClickOpen() {
        this.setState({ open: true });
    }
    handleClose(){
        this.setState({ open: false });
    };
    render(){
        const { email, open, loading, error } = this.state;
        const { classes } = this.props;
        return(
            <div>
                <ForgotPass onClick={this.handleClickOpen} tabIndex="1">
                    ¿Olvidaste tu contraseña?
                </ForgotPass>
                <Dialog
                    open={open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title" >
                    <DialogTitle id="form-dialog-title">Restablecer Contraseña</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Para restablecer tu contraseña,
                            debes ingresar la dirección de correo electrónico vinculada a tu cuenta
                        </DialogContentText>
                        <TextField 
                            autoFocus
                            type="email"
                            icon={"email"}
                            typeStyle="outlined"
                            label="Correo Electrónico"
                            containerStyles={{marginTop: 22}}
                            value={email}
                            onChange={this.handleChange}
                            className={classes.inputStyle} />
                            {error&&(
                                <DialogContentText className={classes.errorDialogText}>
                                    La dirección de correo electrónico no existe.
                                </DialogContentText>)}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancelar
                        </Button>
                        <Button onClick={this.handleSend} color="primary" loading={loading}>
                            Restablecer
                        </Button>
                    </DialogActions>
                    </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(ForgetPassword);