import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Statistics from '../../components/Statistics';
import { Table, TableHead, TableToolbar, TableTabs, TableBody } from '../../components/Tables';
import { getCarousels, deleteCategories, updateAllCarousels, deleteCategoriesImg, getCategories } from '../../firebase/Database';
import CardCarousel from '../../components/CardCarousel';
import { Typography, Button, Select, MenuItem, Divider, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: "hidden"
  },
  demo: {
    overflow: "hidden"
  },
  inputStyle: {
    // margin: "10px 15px",
    width: "100%",
  },
});

class Carouseles extends Component {
    constructor(props){
      super(props);
      this.state = {
        carouseles: {
          carousels: [],
          types: []
        },
        openDelete: false,
        focusId: "",
        openConfirm: false,
        parentKey: "",
        keyChild: "",
        productos: []
      }
    }
    componentDidMount(){
      getCarousels(data => {
        this.setState({carouseles: data.val()});
        console.log(data.val());
      });
      getCategories(data => {
        let _data = data.val();
        let _dataS = [{
            id: 0,
            name: 'Seleccione un Producto'
        }];
        _data.forEach(element => {
            element.products.forEach(product => {
                _dataS.push({
                    id: product.reference_id,
                    name: product.name
                });
            });
        });
        this.setState({productos: _dataS});
      });
    }
    handleRemove = id => {
      this.setState({openDelete: true, focusId: id});
    }
    handleClose = ()=>{
      this.setState({openDelete: false});
    }
    deleteCarousel = ()=>{
      deleteCategories(this.state.focusId, (res)=>{
        console.log(res);
      });
    }
    
    handleChangeAudience = carousel => event => {
      const { carouseles } = this.state;
      let value = event.target.value;
      let car_save = [];
      for(let carouselKeyParent in carouseles){
        let _carousel = carouseles[carouselKeyParent];
        if(typeof _carousel !== "object") continue;
        if(_carousel.layout === value) _carousel.layout = 0;
        if(carouselKeyParent === carousel) _carousel.layout = value;
        car_save[carouselKeyParent] = _carousel;
      }
      updateAllCarousels(car_save, res => {
        console.log(res);
      });
    }
    
    handleDelete = () => {
      const {parentKey, keyChild} = this.state;
      deleteCategoriesImg(parentKey, keyChild, (res)=>{
          this.setState({openConfirm: false});
      });
      
  } 
  updateImagenDelete = (parentKey, keyChild) => {
    this.setState({parentKey, keyChild}, ()=>{
      this.setState({openConfirm: true});
    });
  }

    render() {
      const layoutsTypes = [{name: "Ninguno", id: 0}, {name: "Login", id: 1}, {name: 'Inicio', id: 2}];
      const { carouseles, openDelete, openConfirm, productos } = this.state;
      const { classes } = this.props;
      const carouselesTabs = [];
      for(let carouselKeyParent in carouseles){
        // let carousel = carouseles[carouselKeyParent];
        let _carousel = carouseles[carouselKeyParent];
        let _carouselimgs = {
          images: []
        };
        for(let keyImg in _carousel.images){
          _carouselimgs.images.push({..._carousel.images[keyImg], keyImg});
        }
        if(typeof _carousel !== "object") continue;
        console.log(_carousel);
        carouselesTabs.push({
          title: _carousel.name,
          count: (_carouselimgs.images)?_carouselimgs.images.length:'0',
          content: (
            <div>
              <Grid container spacing={24} style={{paddingTop: 20}}>
                <Grid item xs={5}></Grid>
                <Grid item xs={4}>
                  <Select
                        value={_carousel.layout}
                         onChange={this.handleChangeAudience(carouselKeyParent)}
                        className={classes.inputStyle}
                        name="layout"
                    >
                        {layoutsTypes.map((prop, key) => (
                            <MenuItem key={key} value={prop.id}>
                              {prop.name}
                            </MenuItem>
                        ))}
                    </Select>
                </Grid>
                <Grid item xs={3} style={{textAlign: 'center'}}>
                  <Button variant={"contained"} color={"primary"} onClick={()=> this.handleRemove(carouselKeyParent)}>
                      ELIMINAR
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
              </Grid>
              {((_carouselimgs.images))?_carouselimgs.images.map((element, key) => (
                <div>
                  <CardCarousel 
                    handleDelete={()=>this.updateImagenDelete(carouselKeyParent, element.keyImg)}
                    key={key}
                    element={element}
                    keyChild={element.keyImg}
                    parentKey={carouselKeyParent}
                    imagenUrl={element.url}
                    dateText={element.updatedDate}
                    title={element.title}
                    productos={productos}
                    redirectValue={element.redirect} />
                </div>
              )):<div><p style={{textAlign: 'center'}}>Ninguna imagen disponible</p></div>}
              <div style={{ height: 80 }}></div>
            </div>
          )
        });
      }
      return (
        <div>
          <Grid container className={classes.root}>
            <Grid item xs={12}>
              <Grid
                container
                spacing={0}
                className={classes.demo}
                alignItems={"stretch"}
                direction={"column"}
                justify={"flex-start"} >
                  <Grid item>
                    {/* <Statistics /> */}
                    <TableToolbar title="Carouseles" iconName="view_carousel" />
                    <TableTabs fullWidth tabs={carouselesTabs} />
                  </Grid>
              </Grid>
            </Grid>
          </Grid>
          <div>
              <Dialog
                open={openDelete}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Seguro que quiere eliminar?"}</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    despues de eliminar el Carousel no podra recuperarlo
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose} color="primary">
                    CANCELAR
                  </Button>
                  <Button onClick={()=>{this.setState({openDelete: false}, this.deleteCarousel); }} color="primary" autoFocus>
                    BORRAR
                  </Button>
                </DialogActions>
              </Dialog>
              
                <Dialog
                    open={openConfirm}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    <DialogTitle id="alert-dialog-title">{"Seguro que quiere eliminar?"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        despues de eliminar este elemento no podra recuperarlo
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={()=>this.setState({openConfirm: false})} color="primary">
                        CANCELAR
                    </Button>
                    <Button onClick={this.handleDelete} color="primary" autoFocus>
                        BORRAR
                    </Button>
                    </DialogActions>
                </Dialog>
          </div>
        </div>
      );
    }
}

Carouseles.propTypes = {
  
}

export default withStyles(styles)(Carouseles);