import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Switch from '@material-ui/core/Switch';
import Statistics from '../../components/Statistics';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import { Table, TableHead, TableToolbar, TableTabs, TableBody } from '../../components/Tables';
import { getCategories, getBestCategories, setCategories } from '../../firebase/Database';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  colorSwitchBase: {
    color: green[300],
    '&$colorChecked': {
      color: green[500],
      '& + $colorBar': {
        backgroundColor: green[500],
      },
    },
  },
  colorSwitchBaseRed: {
    color: red[300],
    '&$colorChecked': {
      color: red[500],
      '& + $colorBar': {
        backgroundColor: red[500],
      },
    },
  },
  colorBar: {},
  colorChecked: {},
});

class Categories extends Component {
    constructor(props){
      super(props);
      this.state = {
        loading: true,
        MoredataHeader: [
          { id: 'date_send', styles: {textAlign: "left", width: 105}, disablePadding: true, label: 'Fecha de envío' },
          { id: 'audience', styles: {textAlign: "center"}, stylesHeader: {paddingLeft: 50}, disablePadding: false, label: 'Audiencia' },
          { id: 'message', styles: {textAlign: "center"}, disablePadding: false, label: 'Mensaje' },
        ],
        Moredata: [
          ['12/08/2018', 'CLIENTE', 'KSD JKSD JKJKJKJ KJKSJDK JSKDJ JKSDJGA ASHG FDK AS AGSHAGSAHSGA GASHA SGAH'],
          ['12/08/2018', 'CLIENTE', 'KSD JKSD JKJKJKJ KJKSJDK JSKDJ JKSDJGA ASHG FDK AS AGSHAGSAHSGA GASHA SGAH'],
          ['12/08/2018', 'CLIENTE', 'KSD JKSD JKJKJKJ KJKSJDK JSKDJ JKSDJGA ASHG FDK AS AGSHAGSAHSGA GASHA SGAH'],
          ['12/08/2018', 'CLIENTE', 'KSD JKSD JKJKJKJ KJKSJDK JSKDJ JKSDJGA ASHG FDK AS AGSHAGSAHSGA GASHA SGAH'],
        ],
        dataHeader: [
          { id: 'categori_id', disablePadding: false, label: 'ID', styles: {textAlign: "left"} },
          { id: 'categori_name', disablePadding: false, label: 'Nombre', styles: {textAlign: "left"} },
          { id: 'categori_products', numeric: true, disablePadding: true, label: 'Productos', styles: {textAlign: "center"}, stylesHeader: {paddingRight: 26} },
          { id: 'categori_order', numeric: true, disablePadding: true, label: 'Orden', styles: {textAlign: "center"}, stylesHeader: {paddingRight: 26} },
          { id: 'categori_visible', disablePadding: true, label: 'Visible', styles: {textAlign: "center"} },
          { id: 'categori_home', disablePadding: true, label: 'Home', styles: {textAlign: "center"} },
        ],
        data: []
      }
    }
    renderSwitch = (key, value) => {
      const {classes} = this.props;
      const {dataProducts} = this.state;
      return <Switch
        checked={dataProducts[key][value]}
        onChange={(event)=>{
          this.setState(prev => {
            let _prev = prev;
            _prev.dataProducts[key][value] = !_prev.dataProducts[key][value];
            return _prev;
          }, ()=>{
            setCategories(`${key}/${value}`, dataProducts[key][value], error=>{
              if(error){
                alert("No se pudo actualizar los datos");
              }else{
                //this.setState({loading: false});
              }
            });
          });
        }}
        value="checkedA"
        classes={{
          switchBase: dataProducts[key][value]? classes.colorSwitchBase:classes.colorSwitchBaseRed,
          checked: classes.colorChecked,
          bar: classes.colorBar,
        }}
      />
    }
    componentDidMount(){
      getCategories(data => {
        this.setState({dataProducts: data.val(), loading: false});
      });
      getBestCategories(data => {
        this.setState({carouseles: data.val()});
      });
    }
    render() {
      const { classes } = this.props;
      const { Moredata, MoredataHeader, data, dataHeader, dataProducts, loading } = this.state;

      let _data = [];
        for(let c in dataProducts){
          _data.push([
            (parseInt(c)+1),
            dataProducts[c].name,
            dataProducts[c].products.length,
            dataProducts[c].order_id,
            this.renderSwitch(c, 'shown'),
            this.renderSwitch(c, 'home'),
            //element.home? <Icon style={{color: '#388e3c'}}>check</Icon> : <Icon style={{color: '#d32f2f'}}>close</Icon>,
          ]);
        }

      return (
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Grid
              container
              spacing={0}
              className={classes.demo}
              alignItems={"stretch"}
              direction={"column"}
              justify={"flex-start"} >
                <Grid item>
                  {/* <Statistics /> */}
                  <TableToolbar title="Categorías" iconName="collections_bookmark" />
                  <TableTabs fullWidth tabs={[
                    {
                      title: "Categorias",
                      count: _data.length||'0',
                      content: (
                        <Table aria-labelledby="Categorias" loading={loading} noCheck data={_data.reverse()} columnDataHeader={dataHeader}>
                          <div>
                            <TableHead />
                            <TableBody />
                          </div>
                        </Table>
                      )
                    },
                    // {
                    //   title: "Más vendidos",
                    //   count: Moredata.length||'0',
                    //   disabled: true,
                    //   content: (
                    //     <Table aria-labelledby="Más vendidos" data={Moredata} columnDataHeader={MoredataHeader}>
                    //       <div>
                    //         <TableHead />
                    //         <TableBody />
                    //       </div>
                    //     </Table>
                    //   )
                    // }
                  ]} />
                </Grid>
            </Grid>
          </Grid>
        </Grid>
      );
    }
}

Categories.propTypes = {
    
}

export default withStyles(styles)(Categories);

// {
//   title: "Administrador",
//   count: data.length,
//   content: (
//     <Table aria-labelledby="Administrador" data={data} columnDataHeader={dataHeader}>
//       <div>
//         <TableHead />
//         <TableBody />
//       </div>
//     </Table>
//   )
// }