import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Statistics from '../../components/Statistics';
import { Table, TableHead, TableToolbar, TableTabs, TableBody } from '../../components/Tables';
import { getNotifications, deleteNotification } from '../../firebase/Database';
import { IconButton, Select } from '@material-ui/core';
import { sendNotifications } from '../../firebase/Messaging';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextFieldMaterial from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  inputStyle: {
    width: "100%"
  },
  
  inputCreateService:{
    outline:'none', 
    border:'1px solid #E0E0E0', 
    height:'52px', 
    paddingLeft:'24px', 
    paddingRight: '8px',
    paddingTop:'2px', 
    borderRadius:'3px',
    display: "flex",
    alignItems: "center",
    marginTop: 15
},
});


const currencies = [
  {
    id: 'notificaciones',
    name: 'TODOS',
  },
  {
    id: 'asesores',
    name: 'ASESORES',
  },
  {
    id: 'clientes',
    name: 'CLIENTES',
  },
];

class Notifications extends Component {
    constructor(props){
      super(props);
      this.state = {
        replyMsg: {},
        audienceMsg: "notificaciones",
        deleteNotiId: "",
        openConfirm: false,
        deleteNoti: false,
        loading: true,
        dataHeader: [
          { id: 'date_send', styles: {textAlign: "left", width: 105}, disablePadding: false, label: 'Fecha de envío' },
          { id: 'audience', styles: {textAlign: "center"}, stylesHeader: {paddingLeft: 50}, disablePadding: false, label: 'Audiencia' },
          { id: 'message', styles: {textAlign: "left"}, stylesHeader: {textAlign: "center"}, disablePadding: false, label: 'Mensaje' },
          { id: 'status', styles: {width: 15}, disablePadding: true, label: '' },
          { id: 'action', disablePadding: false, styles: {width: 15}, label: '' },
        ],
        data: [],
        draft: [],
        fullData: [],
        searchData: []
      }
    }
    deleteNotification = id =>{
      this.setState({openConfirm: true, deleteNotiId: id});
    }
    replyNotification = (key, msg) =>{
      this.setState({replyMsg: msg}, ()=>{
        this.setState({openConfirmReply: true});
      });
    }
    componentDidMount(){
      getNotifications(data => {
        let _data = data.val();
        let normal = [];
        let draft = [];
        for(let n in _data){
          let element = _data[n];
          let date = new Date(element.date_create);
          if(!element.draft) normal.push([`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`,
                                            element.audience,
                                            element.message,
                                            <Icon style={{color: 'green'}}>check</Icon>,
                                            <IconButton onClick={()=>this.deleteNotification(n)} color={"primary"} component="span"><Icon>delete</Icon></IconButton >]);
          else draft.push([`${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`,
                            element.audience,
                            element.message,
                            <IconButton onClick={()=>this.replyNotification(n, element)} color={"primary"} component="span"><Icon>reply_all</Icon></IconButton >,
                            <IconButton onClick={()=>this.deleteNotification(element.message, element.title)} color={"primary"} component="span"><Icon>delete</Icon></IconButton >]);
        }
        this.setState({data: normal, draft, loading: false, fullData: _data});
      });
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };  
    handleCloseNoti = () => {
      if(this.state.deleteNoti){
          deleteNotification(this.state.deleteNotiId, ()=>{
            this.setState({deleteNoti: false, deleteNotiId: "", openConfirm: false});
          });
        }
      this.setState({ openConfirm: false });
    }
     
    handleCloseReply = () => {
      const {replyMsg, audienceMsg} = this.state;
      sendNotifications({
        draft: false,
        to: audienceMsg,
        body: replyMsg.message
      });
      this.setState({replyMsg: {}, openConfirmReply: false});
    }
    // search = msg => {
    //   const {fullData} = this.state;
    //   this.setState({searchData: []}, ()=>{
    //     if(fullData){
    //       let _searchData = [];
    //       if(msg.length>0){
    //         for(let n in fullData){
    //         let element = fullData[n];
    //         let date = new Date(element.date_create);
    //         for(let _k in element){
    //           if(typeof element[_k] === 'string' && (element[_k].toUpperCase()).search((msg.toUpperCase()))>0){
    //             console.log(element[_k].toUpperCase(), msg.toUpperCase())
    //             _searchData.push([  `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`,
    //                                 element.audience,
    //                                 element.message,
    //                                 <Icon style={{color: 'green'}}>check</Icon>,
    //                                 <IconButton onClick={()=>this.deleteNotification(n)} color={"primary"} component="span"><Icon>delete</Icon></IconButton >]);
    //               break;
    //             }
    //           }
    //         }
    //         this.setState({searchData: _searchData});
    //       }
    //     }
    //   });
    // }
    search = msg => {
    //   const {fullData} = this.state;
    //   let filtered = fullData.filter((notifications) => {
    //     console.log(notifications)
    //     return notifications.filter(function(noti) {
    //         return msg.indexOf(noti) > -1;
    //     }).length === msg.length;
    //  });
    }
    render() {
      const { classes } = this.props;
      const { dataHeader, data, draft, loading, openConfirm, openConfirmReply, searchData  } = this.state;
      return (
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Grid
              container
              spacing={0}
              className={classes.demo}
              alignItems={"stretch"}
              direction={"column"}
              justify={"flex-start"} >
                <Grid item>
                  {/* <Statistics /> */}
                  <TableToolbar title="Notificaciones" iconName="notifications" searchAction={this.search} />
                  {(searchData.length<1)?
                  <TableTabs fullWidth tabs={[{
                    title: "Programados",
                    count: data.length,
                    content: (
                      <Table loading={loading} noCheck aria-labelledby="tableTitle" data={data} columnDataHeader={dataHeader}>
                        <div>
                          <TableHead />
                          <TableBody />
                        </div>
                      </Table>
                    )
                  }, {
                    title: "Borrador",
                    count: draft.length,
                    content: (
                      <Table loading={loading} noCheck aria-labelledby="tableTitle" data={draft} columnDataHeader={dataHeader}>
                        <div>
                          <TableHead />
                          <TableBody />
                        </div>
                      </Table>
                    )
                  }]} /> : 
                  <TableTabs fullWidth tabs={[{
                    title: "Resultados de la busqueda",
                    count: searchData.length,
                    content: (
                      <Table loading={loading} noCheck aria-labelledby="tableTitle" data={searchData} columnDataHeader={dataHeader}>
                        <div>
                          <TableHead />
                          <TableBody />
                        </div>
                      </Table>
                    )
                  }]} />
                }
                </Grid>
            </Grid>
            <div>
              <Dialog
                open={openConfirm}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Seguro que quiere eliminar?"}</DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    despues de eliminar la notificación no podra recuperarla
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleCloseNoti} color="primary">
                    CANCELAR
                  </Button>
                  <Button onClick={()=>{this.setState({deleteNoti: true}, this.handleCloseNoti); }} color="primary" autoFocus>
                    BORRAR
                  </Button>
                </DialogActions>
              </Dialog>
              
              <Dialog
                open={openConfirmReply}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Seleccione una Audiencia"}</DialogTitle>
                <DialogContent>
                  <Select
                      value={this.state.audienceMsg}
                      onChange={this.handleChange('audienceMsg')}
                      className={classes.inputCreateService}
                      name="audienceMsg"
                  >
                      {currencies.map((prop, key) => (
                          <MenuItem key={key} value={prop.id}>
                          {prop.name}
                          </MenuItem>
                      ))}
                  </Select>
                  {/* <TextFieldMaterial
                        id="select-audience"
                        select
                        label="Audiencia"
                        className={classes.inputStyle}
                        value={this.state.audienceMsg}
                        onChange={this.handleChange('audienceMsg')}
                        SelectProps={{
                            MenuProps: {
                            className: classes.menu,
                            },
                        }}
                        margin="normal"
                        >
                        {currencies.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                            {option.label}
                            </MenuItem>
                        ))}
                    </TextFieldMaterial> */}
                </DialogContent>
                <DialogActions>
                  <Button onClick={()=> this.setState({ openConfirmReply: false })} color="primary">
                    CANCELAR
                  </Button>
                  <Button onClick={this.handleCloseReply} color="primary" autoFocus>
                    ENVIAR
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          </Grid>
        </Grid>
      );
    }
}

Notifications.propTypes = {
  
}

export default withStyles(styles)(Notifications);