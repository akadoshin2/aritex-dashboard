import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Statistics from '../../components/Statistics';
import FullScreenDialog from '../../components/FullScreenDialog';
import { Table, TableHead, TableToolbar, TableTabs, TableBody } from '../../components/Tables';
import { getUsers, setUser, getClients, deleteUser } from '../../firebase/Database';

import {TextField} from '../../components/Material';
import TextFieldMaterial from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import ArrowDropDown from '@material-ui/icons/ExpandMore';
import InputAdornment from '@material-ui/core/InputAdornment';
import Select from '@material-ui/core/Select';
import Files from 'react-files';
import Paper from '@material-ui/core/Paper';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
        padding: "40px 13%",
    },
    formControl: {
        margin: theme.spacing.unit,
    },
    inputStyle: {
       width: "100%"
    },
    menu: {
        width: 200,
    },
    button: {
        margin: "20px 10px",
    },
    inputCreateService:{
        outline:'none', 
        border:'1px solid #E0E0E0', 
        height:'52px', 
        paddingLeft:'24px', 
        paddingRight: '8px',
        paddingTop:'2px', 
        borderRadius:'3px',
        display: "flex",
        alignItems: "center",
        marginTop: 15
    },
});

class Users extends Component {
    constructor(props){
      super(props);
      this.state = {
        loadingBtn: false,
        clientes: [],
        AdmindataHeader: [
          { id: 'email', styles: {textAlign: "left"}, disablePadding: false, label: 'Correo' },
          { id: 'username', styles: {textAlign: "left"}, disablePadding: false, label: 'Usuario' },
          { id: 'first_name', styles: {textAlign: "left"}, disablePadding: false, label: 'Nombres' },
          { id: 'image', styles: {textAlign: "left"}, disablePadding: false, label: 'Imagen' },
          { id: 'active', styles: {textAlign: "left"}, disablePadding: false, label: 'Activo' },
        ],
        Admindata: [],
        SellerDataHeader: [
          { id: 'email', styles: {textAlign: "left"}, disablePadding: false, label: 'Correo' },
          { id: 'username', styles: {textAlign: "left"}, disablePadding: false, label: 'Usuario' },
          { id: 'first_name', styles: {textAlign: "left"}, disablePadding: false, label: 'Nombres' },
          { id: 'image', styles: {textAlign: "left"}, disablePadding: false, label: 'Imagen' },
          { id: 'active', styles: {textAlign: "left"}, disablePadding: false, label: 'Activo' },
        ],
        fullData: [],
        SellerData: [],
          openSelecter: false,
          disabled: true,
          editable: true,
          newUserType: "Admin",
          fullName: "",
          cc: "",
          codigoAsesor: "",
          email: "",
          password: "",
          country: "",
          city: "",
          Czone: "",
          tel2: "",
          tel1: "",
          nameBtn: "EDITAR",
          openConfirm: false
      }
    }
    componentDidMount(){
      console.clear();
      let fullData = null;
      getUsers(data => {
        fullData = data.val();
        let Admindata = [], SellerData = [];
        for(let userId in data.val()){
          let user = data.val()[userId];
          console.log(user, user.is_client);
          if(!user.user.admin){
            SellerData.push([
              user.user.email,
              user.user.username,
              user.seller.name,
              <img width={38} src={user.user.profile} />,
              user.user.status? <Icon style={{color: '#388e3c'}}>check</Icon> : <Icon style={{color: '#d32f2f'}}>close</Icon>,
            ]);
          }else{
            Admindata.push([
              user.user.email,
              user.user.username,
              user.seller.name,
              <img width={38} src={user.user.profile} />,
              user.user.status? <Icon style={{color: '#388e3c'}}>check</Icon> : <Icon style={{color: '#d32f2f'}}>close</Icon>,
            ]);
          }
        }
        console.log(Admindata, SellerData)
        this.setState({Admindata, SellerData, fullData});
      });
    }
    handleClose = ()=>{
      this.setState({openSelecter: !this.state.openSelecter, nameBtn: "EDITAR", editable: true, loadingBtn: false});
    }
    handleDelete = () =>{
        // const {fullData} = this.state;
        // for(let userId in fullData){
        //     if(fullData[userId].user.email === this.state.userSend.user.email){
        //         console.log(userId);
                // deleteUser(userId, ()=>{
                //     this.setState({openConfirm: false});
                //     this.handleClose();
                // });
        //         return 0;
        //     }
        // }
        deleteUser(this.state.userSend.uid, ()=>{
            this.setState({openConfirm: false});
            this.handleClose();
        });
        
    }
    onClickColumn = (val) => {
      const {fullData} = this.state;
      let email = val[0];
      for(let userId in fullData){
        if(fullData[userId].user.email === email){
          let user = fullData[userId];
          console.log("useeeeer", fullData[userId]);
          let clientes = '';
          if(user.user.clientes_list){
            user.user.clientes_list.forEach(element => {
              clientes+=(`${element.client_id},${element.id_sucursal}\n`);
            });
          }
          this.setState({
            userSend: fullData[userId],
            openSelecter: true,
            disabled: true,
            newUserType: (user.user.admin)? 'Admin': 'Asesor',
            fullName: user.seller.name,
            cc: user.seller.cc,
            codigoAsesor: user.seller.seller_code,
            email,
            password: "******",
            country: user.seller.zone,
            city: user.seller.city,
            Czone: user.seller.country,
            tel2: user.seller.phone2,
            tel1: user.seller.phone1,
            clients: clientes
          });
          return 0;
        }
      }
    }
    handleEditable = ()=>{
      const {userSend, newUserType, codigoAsesor, tel1, tel2, clients, Czone, city, country, cc, fullName, clientes} = this.state;
      let user = userSend;
      this.setState({loadingBtn: true});
      user.user.admin = (newUserType==="Admin");
      user.user.seller.is_client = (newUserType==="Asesor");
      user.user.seller.seller_code = codigoAsesor;
      user.user.seller.phone2 = tel2;
      user.user.seller.phone1 = tel1;
      user.user.seller.zone = Czone;
      user.user.seller.city = city;
      user.user.seller.country = country;
      user.user.seller.cc = cc;
      user.user.seller.name = fullName;

      user.seller.is_client = (newUserType==="Asesor");
      user.seller.seller_code = codigoAsesor;
      user.seller.phone2 = tel2;
      user.seller.phone1 = tel1;
      user.seller.zone = Czone;
      user.seller.city = city;
      user.seller.country = country;
      user.seller.cc = cc;
      user.seller.name = fullName;

      let clientes_list = [];
      let send_data = null;
      getClients(data => {
        let fullData = data.val();
        console.clear();
        clients.split('\n').forEach(element => {
          if(element!==''){
            let element_ = element.split(',')||[];
            let sucursal_ = (element_[1].replace(' ', ''));
            for(let cli in fullData){
              let data = fullData[cli];
              let index = 0;
              data.id_sucursal.map((value, i) => {
                if(value === sucursal_){
                  index = i;
                }
              })
              // data.id_sucursal = '';
              if((element_[0].replace(' ', ''))===data.client_id){
                clientes_list.push({
                  sucursal: sucursal_,
                  nombreSu: data.nombre_sucursal[index],
                  nombreAd: data.address_sucursal[index],
                  nombreCi: data.city_sucursal[index],
                  cc: data.client_id,
                  data: data
                });
                break;
              }
            }
          }
        });
        let clientes_list_fin = [];
        clientes_list.forEach(element => {
          let _data = element.data;
          clientes_list_fin.push({
            ..._data,
            id_sucursal: element.sucursal,
            nombre_sucursal: element.nombreSu,
            address_sucursal: element.nombreAd,
            city_sucursal: element.nombreCi
          });
        });
        user.user.clientes_list = clientes_list_fin;
        console.log("aquie", user);
        setUser(user, this.handleClose);
      });
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        }, () => {
            const {fullName, cc, codigoAsesor, email, password, country, city, Czone, tel1, tel2, clients} = this.state;
            if(fullName!=""&&cc!=""&&codigoAsesor!=""&&email!=""&&password!=""&&country!=""&&city!=""&&Czone!="") this.setState({disabled: false});
            else this.setState({disabled: true});
        });
    };  
    render() {
      const serviceType = ['Admin', 'Asesor'];
      const { classes } = this.props;
      const { Admindata, openConfirm, nameBtn, AdmindataHeader, SellerData, SellerDataHeader, disabled, openSelecter, editable } = this.state;
      const {newUserType, fullName, cc, codigoAsesor, email, password, country, city, Czone, tel1, tel2, clients, loadingBtn} = this.state;
      return (
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Grid
              container
              spacing={0}
              className={classes.demo}
              alignItems={"stretch"}
              direction={"column"}
              justify={"flex-start"} >
                <Grid item>
                  {/* <Statistics /> */}
                  <TableToolbar title="Usuarios" iconName="account_circle" />
                  <TableTabs fullWidth tabs={[{
                    title: "Administrador",
                    count: Admindata.length,
                    content: (
                      <Table aria-labelledby="Administrador" data={Admindata} noCheck columnDataHeader={AdmindataHeader}>
                        <div>
                          <TableHead />
                          <TableBody onClickColumn={this.onClickColumn} />
                        </div>
                      </Table>
                    )
                  }, 
                  {
                    title: "Vendedor",
                    count: SellerData.length,
                    content: (
                      <Table aria-labelledby="Vendedor" data={SellerData} noCheck columnDataHeader={SellerDataHeader}>
                        <div>
                          <TableHead />
                          <TableBody onClickColumn={this.onClickColumn}  />
                        </div>
                      </Table>
                    )
                  }, {
                    title: "Cliente",
                    disabled: true,
                    count: 0,
                    content: null
                  }]} />
                </Grid>
            </Grid>
          </Grid>
          <FullScreenDialog open={openSelecter} title={fullName} handleClose={this.handleClose} content={
                    <div  className={classes.container}>
                        <Grid container spacing={24}>
                            <Grid item xs={4}>
                                <Select
                                    value={this.state.newUserType}
                                    onChange={this.handleChange('newUserType')}
                                    className={classes.inputCreateService}
                                    name="newUserType"
                                >
                                    {serviceType.map((prop, key) => (
                                        <MenuItem key={key} value={prop}>
                                        {prop}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="full-name"
                                    label="Nombre Completo"
                                    value={fullName}
                                    onChange={this.handleChange('fullName')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="cc"
                                    label="Cédula"
                                    value={cc}
                                    onChange={this.handleChange('cc')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="codigoAsesor"
                                    label="Código de asesor"
                                    value={codigoAsesor}
                                    onChange={this.handleChange('codigoAsesor')}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="email"
                                    typeStyle="outlined"
                                    id="email"
                                    label="Correo Electronico"
                                    value={email}
                                    disabled
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="password"
                                    typeStyle="outlined"
                                    id="password"
                                    label="Contraseña"
                                    value={password}
                                    disabled
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="country"
                                    label="País"
                                    onChange={this.handleChange('country')}
                                    value={country}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="city"
                                    label="Ciudad"
                                    onChange={this.handleChange('city')}
                                    value={city}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="text"
                                    typeStyle="outlined"
                                    id="Czone"
                                    label="Zona"
                                    onChange={this.handleChange('Czone')}
                                    value={Czone}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="number"
                                    typeStyle="outlined"
                                    id="tel1"
                                    label="Teléfono 1"
                                    onChange={this.handleChange('tel1')}
                                    value={tel1}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextField
                                    type="number"
                                    typeStyle="outlined"
                                    id="tel2"
                                    label="Teléfono 2"
                                    onChange={this.handleChange('tel2')}
                                    value={tel2}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={4}>
                                <TextFieldMaterial
                                    multiline
                                    variant="outlined"
                                    id="clients"
                                    label="Clientes"
                                    onChange={this.handleChange('clients')}
                                    value={clients}
                                    className={classes.inputStyle}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button disabled={disabled||loadingBtn} variant="contained" onClick={this.handleEditable} className={classes.button} color={'primary'}>
                                    GUARDAR
                                </Button>
                                <Button disabled={disabled||loadingBtn} variant="contained" onClick={this.handleClose} className={classes.button}>
                                    CANCELAR
                                </Button>
                                <Button disabled={loadingBtn}  variant="contained" onClick={()=>this.setState({openConfirm: true})} className={classes.button} style={{color: "white", backgroundColor: "#ff3838"}}>
                                    BORRAR
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                }></FullScreenDialog>
                <Dialog
                    open={openConfirm}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                    <DialogTitle id="alert-dialog-title">{"Seguro que quiere eliminar?"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        despues de eliminar el Usuario no podra recuperarlo
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={()=>this.setState({openConfirm: false})} color="primary">
                        CANCELAR
                    </Button>
                    <Button onClick={this.handleDelete} color="primary" autoFocus>
                        BORRAR
                    </Button>
                    </DialogActions>
                </Dialog>
              
        </Grid>
      );
    }
}

Users.propTypes = {
  
}

export default withStyles(styles)(Users);