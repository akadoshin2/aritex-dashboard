import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Switch from '@material-ui/core/Switch';
import Statistics from '../../components/Statistics';
import green from '@material-ui/core/colors/green';
import Button from '@material-ui/core/Button';
import red from '@material-ui/core/colors/red';
import { Table, TableHead, TableToolbar, TableTabs } from '../../components/Tables';
import TableBody from '@material-ui/core/TableBody';
import { getPedidos, setPedidos, getBestCategories, setCategories } from '../../firebase/Database';
import { ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails, TableRow, TableCell, List, ListItem, Avatar, ListItemText, ListItemSecondaryAction } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
  import TextField from '@material-ui/core/TextField';
  import {default as fetchSend} from 'cross-fetch';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  colorSwitchBase: {
    color: green[300],
    '&$colorChecked': {
      color: green[500],
      '& + $colorBar': {
        backgroundColor: green[500],
      },
    },
  },
  colorSwitchBaseRed: {
    color: red[300],
    '&$colorChecked': {
      color: red[500],
      '& + $colorBar': {
        backgroundColor: red[500],
      },
    },
  },
  colorBar: {},
  colorChecked: {},
});

class Pedidos extends Component {
    constructor(props){
      super(props);
      this.state = {
        loading: true,
        dataHeader: [
          { id: 'pedido_id', disablePadding: false, label: 'ID Orden', styles: {textAlign: "center"} },
          { id: 'observations', disablePadding: false, label: 'Observaciones', styles: {textAlign: "center"} },
          { id: 'pedidos_cantidad', disablePadding: false, label: 'Cantidad de productos', styles: {textAlign: "center"} },
          { id: 'valor_total', numeric: true, disablePadding: true, label: 'Valor Total', styles: {textAlign: "center"}, stylesHeader: {paddingRight: 26} },
          // { id: 'categori_order', numeric: true, disablePadding: true, label: 'Orden', styles: {textAlign: "center"}, stylesHeader: {paddingRight: 26} },
        ],
        data: [],
        pedido: [],
        openSendPedido: false,
        IndBackordeDocto: 6,
        diaEntrega: '',
        numberDiasEntrega: 0,
        description: ''
      }
    }
    componentDidMount(){
      getPedidos(data => {
        this.setState({dataPedidos: data.val(), loading: false});
      });
    }
    handleChange = event => {
      this.setState({ [event.target.name]: event.target.value });
    };
    handleClose = () => {
      this.setState({
        openSendPedido: false
      });
    }
    selectPedido = (pedido) => {
      this.setState({
        pedido,
        openSendPedido: true
      });
    }
    Transition = (props) => {
      return <Slide direction="up" {...props} />;
    }
    sendPedidoRequest = ()=>{
      let pedido_ = this.state.dataPedidos[this.state.pedido[0]];
      pedido_.status = 2;
      // pedido_.indBackordeDocto = this.state.IndBackordeDocto;
      pedido_.num_entrega = this.state.numberDiasEntrega;
      let date_entrega1 = this.state.diaEntrega.replace('-', '');
      pedido_.date_entrega = date_entrega1.replace('-', '');
      console.log(pedido_);
      setPedidos(this.state.pedido[0], pedido_, (res)=>{
        console.log(res);
        this.setState({
          openSendPedido: false
        });
      });
    }
    sendPedidoRequest2 = ()=>{
      let date = new Date();
      let diaEntrega = this.state.diaEntrega.split('-');
      let date_ = `${("0"+date.getDate()).substr(-2)}${("0"+(date.getMonth()+1)).substr(-2)}${date.getFullYear()}`;
      let date_entrega = `${diaEntrega[2]}${diaEntrega[1]}${diaEntrega[0]}`;
      // eslint-disable-next-line radix
      let uiid = parseInt((new Date().getTime())/100);

      //Header
      // <TerceroVend>52964206</TerceroVend>

      let bodyXml = `
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:gen="http://generictransfer.com/">
        <soapenv:Header/>
        <soapenv:Body>
          <gen:ImportarDatosXML>
              <gen:idDocumento>55766</gen:idDocumento>
              <!--Optional:-->
              <gen:strNombreDocumento>Pedido</gen:strNombreDocumento>
              <gen:idCompania>2</gen:idCompania>
              <!--Optional:-->
              <gen:strCompania>1</gen:strCompania>
              <!--Optional:-->
              <gen:strUsuario>gt</gen:strUsuario>
              <!--Optional:-->
              <gen:strClave>gt</gen:strClave>
              <!--Optional:-->
              <gen:strFuenteDatos><![CDATA[
              <MyDataset>
                <Pedidos>
                  <CentroOperacioDoctoc>001</CentroOperacioDoctoc>
                  <NumDocto>1</NumDocto>
                  <FechaDocto>${date_}</FechaDocto>
                  <IndBackordeDocto>${this.state.IndBackordeDocto}</IndBackordeDocto>
                  <TercerFact>${this.state.pedido[4]}</TercerFact>
                  <SucursalFact>${this.state.pedido[7]}</SucursalFact>
                  <TerceroDesp>${this.state.pedido[4]}</TerceroDesp>
                  <SucursalDesp>${this.state.pedido[7]}</SucursalDesp>
                  <CentroOperFac>001</CentroOperFac>
                  <FechaEntrega>${date_entrega}</FechaEntrega>
                  <NumDiasEntrega>${this.state.numberDiasEntrega}</NumDiasEntrega>
                  <OrdenCompra>OC${uiid}</OrdenCompra>
                  <CondPago>60D</CondPago>
                  <Observaciones>${this.state.pedido[1]}</Observaciones>
                </Pedidos>
                ${this.state.pedido[2].map((producto => {
                  let valPedidos = 0, calCount = 0, tallas = "", color = "";
                  producto.variations.forEach(element => {
                    valPedidos+=element.quantity;
                    calCount+=element.quantity;
                    if(tallas.search(element.size)<0) tallas+=`${element.size}, `;
                    if(color.search(element.color)<0) color+=`${element.color}, `;
                  });
                  return(`
                  <MovtoPedidos>
                    <CentroOper>001</CentroOper>
                    <ConsecDocto>1</ConsecDocto>
                    <NumReg>1</NumReg>
                    <Item>${producto.product.reference_id}</Item>
                    <Bodega>00101</Bodega>
                    <Motivo>01</Motivo>
                    <CentroOperMvto>001</CentroOperMvto>
                    <UnidNegocMvto>99</UnidNegocMvto>
                    <FechaEntrga>${date_entrega}</FechaEntrga>
                    <NumDiasEntrega>${this.state.numberDiasEntrega}</NumDiasEntrega>
                    <Cantidad>${calCount}</Cantidad>
                    <IndBackorder>${this.state.IndBackordeDocto}</IndBackorder>
                  </MovtoPedidos>
                `);}))}
                </MyDataset>]]>
              </gen:strFuenteDatos>
              <!--Optional:-->
              <gen:Path>C:\inetpub\wwwroot\GTIntegration\Planos</gen:Path>
          </gen:ImportarDatosXML>
        </soapenv:Body>
      </soapenv:Envelope>`;
      
      console.log(this.state.pedido);
      
      console.log(bodyXml);

      let header = new Headers({
        "content-type": "text/xml",
        "soapaction": "http://generictransfer.com/ImportarDatosXML",
        "host": "190.85.249.115",
        "cache-control": "no-cache"
      });
      // fetchSend('http://190.85.249.115/GTIntegration/ServiciosWeb/wsGenerarPlano.asmx', {
      //   method: 'POST',
      //   headers: header,
      //   body: JSON.stringify(bodyXml)
      // }).then(res => {
      //   console.log(res);
      // });
      fetch('http://localhost:5000/api/v1/todos', {
        // method: 'POST',
        // headers: header,
        // body: JSON.stringify(bodyXml)
      })
      .then(user => {
        console.log(user);
      })
      .catch(err => {
        console.error(err);
      });
    }
    render() {
      const { classes } = this.props;
      const { data, dataHeader, dataPedidos, loading } = this.state;

      let _data = [];
        for(let c in dataPedidos){
          _data.push([
            c,
            dataPedidos[c].observation,
            dataPedidos[c].products,
            dataPedidos[c].total.toLocaleString('es-CO', {
                style: 'currency',
                currency: 'COL',
              }),
              dataPedidos[c].client_id,
              dataPedidos[c].date,
              dataPedidos[c].client_name,
              dataPedidos[c].sucursal_id,
              dataPedidos[c].status
            // dataProducts[c].products.length,
            // dataProducts[c].order_id
          ]);
        }
        console.log("PEDIDOSSSSS", _data);
        console.log("PEDIDOSSSSS 2", dataPedidos);

      return (
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Grid
              container
              spacing={0}
              className={classes.demo}
              alignItems={"stretch"}
              direction={"column"}
              justify={"flex-start"} >
                <Grid item>
                  {/* <Statistics /> */}
                  <TableToolbar title="Pedidos" iconName="assignment" />
                  {/* <TableTabs fullWidth tabs={[
                    {
                      title: "Pedidos",
                      count: _data.length||'0',
                      content: ( */}
                        {/* // <Table aria-labelledby="Pedidos" loading={loading} noCheck data={_data.reverse()} columnDataHeader={dataHeader}> */}
                          <div>
                            <div style={{padding: 20}}>
                              {_data.map((pedido, key) => {
                                let date = new Date(pedido[5]);
                                return (
                                <ExpansionPanel>
                                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                    <Typography className={classes.heading}>Pedido ({date.getDate()}/{date.getMonth()+1}/{date.getFullYear()}) <small style={{color: 'grey'}}>{pedido[3]}</small></Typography>
                                  </ExpansionPanelSummary>
                                  <ExpansionPanelDetails>
                                    <div style={{width: '100%'}}>
                                      <List>
                                        {pedido[2].map(value => { 
                                          let valPedidos = 0, calCount = 0, tallas = "", color = "";
                                          value.variations.forEach(element => {
                                            valPedidos+=element.quantity;
                                            calCount+=element.quantity;
                                            if(tallas.search(element.size)<0) tallas+=`${element.size}, `;
                                            if(color.search(element.color)<0) color+=`${element.color}, `;
                                          });
                                          valPedidos = valPedidos*value.product.price;
                                          return(
                                          <div>
                                            <ListItem key={value} dense button className={classes.listItem}>
                                              <Avatar alt="Remy Sharp" src={value.product.images[0].src} />
                                              <ListItemText primary={`${value.product.name}  -  (x${calCount})`} secondary={`Ref ${value.product.reference}`} />
                                              <ListItemSecondaryAction>
                                                <ListItemText primary={`C/U: ${value.product.price.toLocaleString('es-CO', {style: 'currency', currency: 'COL',})}`} secondary={`Total: ${valPedidos.toLocaleString('es-CO', {style: 'currency', currency: 'COL',})}`} />
                                              </ListItemSecondaryAction>
                                            </ListItem>
                                            <Typography className={classes.heading}>Tallas: {tallas}</Typography>
                                            <Typography className={classes.heading}>Colores: {color}</Typography><br />
                                          </div>
                                        )})}
                                      </List>
                                      <hr />
                                      <Typography className={classes.heading} style={{color: 'grey'}}>ID: {pedido[0]}</Typography>
                                      <Typography className={classes.heading}>Cliente: {pedido[6]} <small style={{color: 'grey'}}>({pedido[4]})</small></Typography>
                                      <Typography className={classes.heading}>Descripción: {pedido[1]}</Typography><br /><br />
                                      <Button disabled={(pedido[8] == 2||pedido[8] == 1)? 'disabled': ''} variant="outlined" color="primary" className={classes.button} onClick={(pedido[8] == 2||pedido[8] == 1)? ()=>null: ()=>this.selectPedido(pedido)}>
                                        {(pedido[8] == 2)? 'En progreso': ''}
                                        {(pedido[8] == 1)? 'Enviado a Siesa.': ''}
                                        {(pedido[8] == 3)? 'Error - Volver a intentar': ''}
                                        {(pedido[8]>3||pedido[8]==undefined)? 'Finalizar Pedido': ''}
                                      </Button>
                                    </div>
                                  </ExpansionPanelDetails>
                                </ExpansionPanel>
                              )})}
                            </div>
                          </div>
                        {/* // </Table> */}
                      {/* )
                    }
                  ]} /> */}
                </Grid>
            </Grid>
          </Grid>
          <Dialog
            open={this.state.openSendPedido}
            TransitionComponent={this.Transition}
            keepMounted
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">
              {"Finalizar el pedido"} #{this.state.pedido[0]}
            </DialogTitle>
            <DialogContent>
              {/* <Select
                style={{width: '100%'}}
                value={this.state.IndBackordeDocto}
                onChange={this.handleChange}
                inputProps={{
                  name: 'IndBackordeDocto',
                  id: 'select-IndBackordeDocto',
                }}
              >
                <MenuItem value={6}>
                  <em>Opciones de despacho</em>
                </MenuItem>
                <MenuItem value={0}>Despachar solo lo disponible</MenuItem>
                <MenuItem value={1}>Despachar solo lo disponible y lo demás cancele</MenuItem>
                <MenuItem value={2}>Despachar sólo lineas completas y demás pendientes</MenuItem>
                <MenuItem value={3}>Despachar sólo lineas completas y demás cancele</MenuItem>
                <MenuItem value={4}>Despachar sólo pedido completo</MenuItem>
                <MenuItem value={5}>Por línea</MenuItem>
              </Select><br /><br /> */}
              <TextField
                style={{width: '100%'}}
                onChange={event => {
                  this.setState({
                    diaEntrega: event.target.value
                  })
                }}
                id="fecha_entrega"
                label="Fecha de entrega"
                type="date"
                defaultValue={this.state.diaEntrega}
                InputLabelProps={{
                  shrink: true,
                }}
              />
              <TextField
                id="diasEntrega-number"
                label="Numero de dias a entregar"
                value={this.state.numberDiasEntrega}
                style={{width: '100%'}}
                onChange={event => {
                  this.setState({
                    numberDiasEntrega: event.target.value
                  })
                }}
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
              />
              {/* <TextField
                id="description-textarea"
                label="Descripción"
                placeholder="Descripción"
                multiline
                margin="normal"
                value={this.state.description}
                style={{width: '100%'}}
                onChange={event => {
                  this.setState({
                    description: event.target.value
                  })
                }}
              /> */}
              <br /><br />
              <DialogContentText id="alert-dialog-slide-description_2">
                * Validar bien los datos antes de enviar el pedido.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancelar
              </Button>
              <Button onClick={this.sendPedidoRequest} color="primary">
                Enviar
              </Button>
            </DialogActions>
          </Dialog>
        </Grid>
      );
    }
}

Pedidos.propTypes = {
    
}

export default withStyles(styles)(Pedidos);