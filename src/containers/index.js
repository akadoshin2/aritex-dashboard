import React from 'react';
import Main from './Main';
import Login from './Login';
import P404 from './Errors/404';
import Categories from './Categories';
import Users from './Users';
import Notifications from './Notifications';
import Carousels from './Carousels';
import Pedidos from './Pedidos';

export default {
    Main,
    Login,
    P404,
    Categories,
    Users,
    Notifications,
    Carousels,
    Pedidos
}