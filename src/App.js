import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Routes from './routes';
import Components from './containers';

import Header from './components/Header';
import Footer from './components/Footer';
import Main from './components/Main';
import { firebase } from './firebase';


const PrivateRoute = ({ component: Component, ...props, authUser, pathLocation }) => (
  <Route
    {...props}
    render={props =>
      authUser? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: Routes.find((route) => route.name === pathLocation).path,
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#7953d2',
      main: '#4527a0',
      dark: '#000070',
      contrastText: '#ffffff',
    },
    secondary: {
      light: '#534bae',
      main: '#1a237e',
      dark: '#000051',
      contrastText: '#ffffff',
    },
  },
  });

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      authUser: null
    }
  }
  componentDidMount(){
    firebase.auth.onAuthStateChanged(authUser=>{
      this.setState({authUser: Boolean(authUser), loading: false});
    })
  }
  getRoutes = ()=> 
      <Switch>
        {Routes.map((page, key) => {
            if(page.isRoute&&page.enabled){
                if(page.onlyNoAuth){
                  return(
                    <PrivateRoute
                        key={key}
                        exact path={page.path}
                        authUser={!this.state.authUser}
                        pathLocation="HOME"
                        component={Components[page.component]}
                    />
                  );
                }
                if(page.private){
                  return(
                    <PrivateRoute
                        key={key}
                        exact path={page.path}
                        authUser={this.state.authUser}
                        pathLocation="LOGIN"
                        component={Components[page.component]}
                    />
                  );
                }
                return(
                  <Route
                      key={key}
                      exact path={page.path}
                      component={Components[page.component]}
                  />
                );
                
            }
        })}
        <Route component={Components.P404} />
      </Switch>

  render() {
    if(this.state.loading){
      return(
        <Grid container style={{flexGrow: 1}}>
            <Grid xs={12}
                container
                spacing={16}
                style={{height: "100vh"}}
                alignItems={"center"}
                direction={"column"}
                justify={"center"} >
                <Grid item>
                  <CircularProgress size={60} />
                </Grid>
                <Grid item>
                  <Typography color="primary" variant="subheading" align="center">
                    Aritex
                  </Typography>
                </Grid>
            </Grid>
        </Grid>
        );
    }
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          {(this.state.authUser)? 
          <div>
            <Header />
            <Main>
              {this.getRoutes()}
            </Main>
            <Footer />
          </div>: this.getRoutes()}
        </Router>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {

}
const mapDispatchToProps = dispatch => ({
  
});
export default connect(null, mapDispatchToProps)(App);
